<h1> Cambiar imagen de mi tienda</h1>
<?php if(isset($error)):?>
<div class="alert-error alert"><?php echo $error;?></div>

<?php endif;?>
<div id="changeshopimage">
  <p>Aqui puedes cambiar la imagen de tu tienda, para aprovechar el 100 por ciento del espacio te recomendamos que sea de 360px de ancho y 360px de alto y tener un tamaño menor de 1mb</p>

  <?php if ($shop->banner):?>
  <div id="changesshopbanner">
  <img src ="<?php echo base_url("images/shop/370x370/$shop->id/$shop->banner");?>" />
  </div>
  <?php else:?>
     <p>Woops aun no tienes banner subele uno!</p>
  <?php endif;?>
  <?php echo form_open_multipart('shops/do_upload');?>

  <input type="file" name="userfile" size="20" />


  <input class="boton" id="botonshopimage" type="submit" value="upload" />

  </form>
</div>