<h1> Crea tu propia tienda! </h1>
<?php if ($this->session->flashdata("errors")!= null):?>
   <ul>
   <?php foreach ($this->session->flashdata("errors") as $error): ?>
   <li><span class = "error"><?php echo $error;?></span></li>
   <?php endforeach;?>
   </ul>
<?php endif;?>

<?php echo form_open('shops/save_new', "id = 'completenew'");?>
<?php echo form_hidden("profile_id", $user->profile->id);?>
<table>
<tr>
<td class="space"><span class="spanform">El nombre de tu tienda: </span></td>
<td><?php echo form_input("name",$this->session->flashdata("name"));?></td>
</tr>
<tr>
<td class="space"><span class="spanform">Descripcion de tu tienda:</span></td>
<td><?php echo form_textarea('description',$this->session->flashdata("name"));?></td>
</tr>
<tr>
<td class="space"></td>
<td><?php echo form_submit("submit", "Crear");?></td>
</tr>
</table>
<?php echo form_close();?>