<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends Application {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
	  $data['products'] = Product::all();
      $yield = $this->load->view('welcome_message', $data);
      //$this->load->view("layouts/application", array('yield' => $yield));
	}
	
	public function category(){
	   $name = $this->uri->segment(2);
	   $data['categories'] = Category::find('all', array('order' => 'name ASC'));
      $data['criteria'] = $name;
      $data['featured'] = Product::find('all', array('conditions' => array("featured" => 1),"limit" => 4, "order" => "RAND()"));
	  $category = Category::find_by_name_url($name);
	   $category = Category::find_by_name_url($name);
	   
	   $data['pagetitle'] = $category->name;
	   $data['product_category'] = $category->products;
	   
	   if($category){
      	$yield = $this->load->view('categories/show', $data, true);
      	$this->load->view("layouts/application", array('yield' => $yield));
	   }
	   else{
	      redirect('404');
	   }
	}
	public function categories(){
	   //$name = $this->uri->segment(2);
        //$criteria = "categories"
        //$data['products'] = Product::find('all', array('conditions' => "name LIKE '%".$criteria."%'"));
        //this must be in a sidebar;
        $data['pagetitle'] = 'Categorías';
        $data['categories'] = Category::find('all', array('order' => 'name ASC'));
        $data['criteria'] = "categories";
        $data['featured'] = Product::find('all', array('conditions' => array("featured" => 1),"limit" => 4, "order" => "RAND()"));
        //$data['featured'] = Product::find_all_by_featured(1);         
	   	$category = Category::find('all', array('order' => 'name ASC'));
      	//$data['products'] = $category->products;
      	//$data['categories'] = $category;
      	$yield = $this->load->view('categories/index', $data, true);
        $this->load->view("layouts/application", array('yield' => $yield));
	}
	
	public function example(){
	   $data['roo'] = "roo";
	   $yield = $this->load->view("welcome_message", $data, true);
	   $this->load->view("layouts/application", array('yield' => $yield));
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */