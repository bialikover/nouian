<h1> Preguntas que me han hecho</h1>
<div id="questions">
  <?php if(isset($error)):?>
  <div class="alert-error alert"><?php echo $error;?></div>
  <?php endif;?>
  <?php if( ! empty($questions)):?>
     <?php foreach ($questions as $question):?>
	      <div id="questionitem">
		      <div id="questionsimage">
		        <?php if ( $question->product->images!= null):?>
              <?php echo anchor("producto/".$question->product->name_url, 
                  '<img id="productpanelimg" width = "100" height = "100" src = "'.base_url() ."images/product/100x100". $question->product->images[0]->image_url.'"/>'
               );?>
            <?php else:?>
      	      <?php echo anchor("producto/".$question->product->name_url, 
      	        "<img src='".base_url()."/assets/images/noimage".rand(0,4).".png'>" );?>
      	    <?php endif;?>
		      </div>
		        <div id="questionsproducttitle">
		        	<h4><?php echo anchor("producto/".$question->product->name_url, $question->product->name );?></h4>
		        </div>
		        <div id="questionsproduct">
		        	<ul id="questionnoanswer">
		        	  <li id="questionpanel">
		        	    Pregunta: <?php echo $question->content;?>
		        	    <span class = "timepanel">
		        	      <?php echo RelativeTime($question->made_at);?> 
		        	    </span>
		        		</li>

		        		<li id="answerpanel">
		        		  <?php echo form_open("questions/update");?>

                   <?php echo form_hidden("id", $question->id);?>
                   <?php echo form_textarea("answer", "", "id= 'answerpanelimput'");?>
                   <?php echo form_submit("submit", "responder", "class='boton'");?>
                   <?php echo form_close();?>
		        		</li>
		        	</ul>
		        </div>
	        </div>
      <?php endforeach;?>
	<?php else:?>
     <h3>Aun no tienes preguntas, promuevelos!!!<h3>
  <?php endif;?>
</div>
