<ul id="navigation">
<?php
	if(logged_in())
	{
	?>
	   <li><?php echo anchor( '/', "Inicio")?></li>
		<li><?php echo anchor('admin/dashboard', 'Dashboard'); ?></li>
		<li><?php if(user_group('admin') === TRUE) { echo anchor('admin/users/manage', 'Manage Users'); } ?></li>
		<li><?php if(user_group('admin') === TRUE) { echo anchor('admin/tags', 'Manage Tags'); } ?></li>
		<li><?php if(user_group('admin') === TRUE) { echo anchor('admin/categories', 'Manage Categories'); } ?></li>
		<li><?php echo anchor('shops/my_shop', 'vender')?></li>
		<li><?php echo anchor('admin/orders/my_orders', 'Mis compras')?></li>
		<li><?php echo anchor('questions/i_made', 'Mis preguntas')?></li>		
		<li><?php if(user_group('editor') === TRUE) { echo anchor('admin/orders/my_sells', 'Mis ventas'); } ?></li>
		<li><?php if(user_group('editor') === TRUE) { echo anchor('questions/made_to_me', 'Responder preguntas'); } ?></li>
		<li><?php echo anchor('logout', 'Logout'); ?></li>
	<?php
	}
	else
	{
	?>
		<li><?php echo anchor('login', 'Login'); ?></li>
		<li><?php echo anchor('register', 'Register'); ?></li>
	<?php
	}
	
?>
</ul>