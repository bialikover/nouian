<h2>Mi tienda</h2>
<div id="tienda-header">
	<div id="tienda-izq">
	   <?php if($shop->banner):?>
         <img src ="<?php echo base_url("images/shop/370x370/$shop->id/$shop->banner");?>" />
         <div id="changebanner"><a href="../shops/edit_shop"><p>Cambia el banner de tu tienda</p></a></div>
      <?php else:?>
		 <img src ="<?php echo base_url(). 'assets/images/noimage'.rand(0,4).'.png';?>" />
         <div id="changebanner"><a href="../shops/edit_shop"><p>Sube un banner a tu tienda</p></a></div>
      <?php endif;?>	   
	</div>
	<div id="tienda-der">
		<div id="descripcion-tienda">			
			<div id="nombre-vendedor">				
			<h2><?php echo $shop->name;?></h2>  
			<span id="triangle"></span>
			</div>
			<div id="descripcion-vendedor"><br>
				<div id="descripcion-vend-contenedor">
				<p><?php echo $shop->description;?></p>
				<p class="credit">Credito: $<?php echo $shop->credit->ammount;?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class = "clear"></div>
<div id="explore-shops">
   <?php if( count($shop->products) !== 0):?>
      <?php foreach($shop->products as $product):?>
		<div id="cont-shop">
			<a href="<?php echo base_url()."producto/".$product->name_url?>">
			<div id="cont-imagen">
			   <?php if($product->images!= null):?>
			      <img src="<?php echo base_url()."images/product/370x370". $product->images[0]->image_url;?>"/>
			   <?php else:?>
			      <img src="<?php echo base_url(). 'assets/images/noimage'.rand(0,4).'.png';?>"/>
			   <?php endif;?>
			</div>
			<div id="spacebck">
			<div id="shop-text">
			<span id="shop-title"><?php echo $product->name;?> </span>
			<span id="shop-title">$<?php echo $product->price;?></span><br>
			<span id="short-descript"><?php echo substr($product->description, 0, 32)."...";?></span>
			<span class="botonshop"> > </span>   
			</div>
			</div>
			</a>
		</div>		
	<?php endforeach;?>
	<div id="cont-shop">
			<a href="../products/create_new">
			<div id="cont-imagen">
			<img src="<?php echo base_url(). 'assets/images/plus.jpg';?>"/>
			</div>
			</a>
	</div>
	<?php else:?>
	   <div id="cont-shop">
			<a href="../products/create_new">
			<div id="cont-imagen">
			<img src="<?php echo base_url(). 'assets/images/plus.jpg';?>"/>
			</div>
			</a>
	</div>
	<?php endif;?>
</div><!--Termina explore-shop=-->
<div class="clear"></div>