<div id="terms">
<h1>Terminos</h1>

<p>Mediante el uso y acceso a este sitio web, http://www.nouian.com (denominados colectivamente el "Sitio" o "Nouian" en estas Condiciones de servicio), usted ("usted", "usuario" o "usuario final ") acepta estos T&eacute;rminos de Servicio (en adelante, los" T&eacute;rminos de Servicio "o" Acuerdo ").</p>

<p>SI USTED NO EST&Aacute; DE ACUERDO CON LOS T&Eacute;RMINOS DE ESTE ACUERDO, NO ACCEDA A ESTE SITIO.</p>

<p><strong>Propiedad Intelectual.</strong><br />Usted reconoce y acepta que todos los contenidos y la informaci&oacute;n en el Sitio est&aacute; protegido por derechos de propiedad y leyes.</p>

<p><strong>Los sitios de terceros.</strong><br />El Sitio puede contener enlaces a otros sitios web mantenidos por terceros. Estos enlaces se proporcionan &uacute;nicamente para su conveniencia y no implica la aprobaci&oacute;n de, o asociaci&oacute;n con el partido por Nouian.</p>

<p><strong>Las modificaciones de este Acuerdo.</strong><br />Nouian se reserva el derecho de cambiar o modificar cualquiera de los t&eacute;rminos y condiciones contenidos en este Acuerdo en cualquier momento. Usted reconoce y acepta que es su responsabilidad revisar el Sitio y estos T&eacute;rminos de Servicio de vez en cuando. Su uso continuado del Sitio despu&eacute;s de tales modificaciones a este Acuerdo constituir&aacute; reconocimiento de los T&eacute;rminos de uso modificados y el acuerdo en cumplir y estar obligado por los nuevos T&eacute;rminos de Servicio.</p>

<p><strong>Terminos de Uso.</strong><br />Nouian tendr&aacute; el derecho de interrumpir o suspender, a su discreci&oacute;n, su acceso a todo o parte del Sitio, con o sin previo aviso y por cualquier raz&oacute;n.</p>

<p><strong>Renuncia de Garant&iacute;a</strong><br />Usted acepta expresamente que el uso del Sitio es bajo su propio riesgo y discreci&oacute;n. El Sitio y todo el contenido y otra informaci&oacute;n contenida en el Sitio se proporciona "TAL CUAL" y "SEG&Uacute;N SU DISPONIBILIDAD" sin garant&iacute;a de ning&uacute;n tipo, ya sea expresa o impl&iacute;cita. Nouian no garantiza que (i) el Sitio y el contenido o la informaci&oacute;n ser&aacute; ininterrumpido, oportuno, seguro o libre de errores, (II) los resultados que puedan obtenerse del uso de este Sitio ser&aacute; eficaz, precisa y fiable. El Sitio puede incluir errores t&eacute;cnicos, imprecisiones o errores tipogr&aacute;ficos. Nouian se reserva el derecho de modificar el contenido del sitio y de la informaci&oacute;n en cualquier momento sin previo aviso.</p>

<p><strong>Limite de Responsabilidad.</strong><br />En ning&uacute;n caso Nouian o sus filiales ser&aacute;n responsables de da&ntilde;os indirectos, incidentales, especiales, punitivos o da&ntilde;os consecuentes de ning&uacute;n tipo, ni de ning&uacute;n tipo que surja de o relacionados con su uso del Sitio, el contenido y otra informaci&oacute;n obtenida en el mismo .</p>

<p>Algunas jurisdicciones proh&iacute;ben la exclusi&oacute;n o limitaci&oacute;n de responsabilidad por da&ntilde;os consecuenciales o incidentales, por lo tanto, las limitaciones anteriores pueden no aplicarse en su caso.</p>

<p><strong>Ley aplicable.</strong><br />Cualquier disputa que surja de o est&eacute; relacionada con estas Condiciones de servicio y / o cualquier otro uso que usted haga del Sitio se regir&aacute;n por las leyes de la M&eacute;xico, sin tener en cuenta los conflictos de disposiciones legales en el mismo.</p>

<p><strong>Fecha de la &uacute;ltima actualizaci&oacute;n.</strong><br />Este acuerdo se actualiz&oacute; por &uacute;ltima vez el 12 de septiembre de 2012.</p>

<p>Publicado con autorizaci&oacute;n de Termsfeed.</p>
</div>
