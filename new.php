<div id="upload-form">
	<div id="upload-form-content">

      <h1> Crea un nuevo producto</h1>
      <h2>Paso 1 ingresa la informacion de tu producto</h2>
      <?php if (isset($errors)):?>
         <ul>
         <?php foreach ($errors as $error): ?>
         <li><span class = "error"><?php echo $error;?></span></li>
         <?php endforeach;?>
         </ul>
      <?php endif;?>

      <?php echo form_open_multipart('products/save_new');?>
         <h2>Elige la categoria..
         <select name = "category_id">
            <?php foreach($categories as $category):?>
               <option value ="<?php echo $category->id;?>"><?php echo $category->name;?></option>
            <?php endforeach;?>
         </select>
         </h2>
	         <h2>El nombre del producto <?php echo form_input("name");?></h2>
            <h2>cantidad<?php echo form_input("quantity");?></h2>
            <h2>precio<?php echo form_input("price");?></h2>
            <h2>descripcion:<?php echo form_textarea('description');?></h2>
            <h2>destacar producto? <?php echo form_checkbox("featured", "1");?></h2>
            <h2>opciones de pago:</h2>
            <?php echo form_checkbox("payment1", "deposito bancario");?>deposito bancario
            <?php echo form_checkbox("payment2", "a convenir");?>a convenir con el cliente
            <h2>opciones de envio</h2>   
            <?php echo form_checkbox("delivery1", "contra entrega");?>contra entrega
            <?php echo form_checkbox("delivery2", "paqueteria");?>(Fedex, multipack, dhl, etc..)
            <h2>material <?php echo form_input("material");?></h2>
            <h2>color<?php echo form_input("color");?></h2>
            <h2>acabado<?php echo form_input("finished");?></h2>
            <?php echo form_submit("submit", "crear");?>
         <?php echo form_close();?>
         
      </div>
</div>