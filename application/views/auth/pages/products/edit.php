<div id="upload-form">
	<div id="upload-form-content">
		<h1>Editar producto!</h1>
		<?php if (isset($errors)):?>
		   <ul>
		   <?php foreach ($errors as $error): ?>
		   <li><span class = "error"><?php echo $error;?></span></li>
		   <?php endforeach;?>
		   </ul>
		<?php endif;?>
		<table>
			<?php echo form_open_multipart('products/edit_save');?>
			<?php echo form_hidden("product_id", $product->id);?>
		  <tr>
		   <td colspan="2"><h2 style="text-align:center; font-size: 22px;"><?php echo $product->name?></h2></td>
		  <!-- <td><?php //echo form_input("name", $product->name);?></td>-->
		   </tr>
		  <tr>
		  <!--<td><h2>Elige la categoria..</h2></td>
		   <td><select name = "category_id">
			  <option value ="<?php //echo $product->category->id;?>"><?php echo $product->category->name;?></option>
			  <?php // foreach($categories as $category):?>
				 <option value ="<?php //echo $category->id;?>"><?php echo $category->name;?></option>
			  <?php //endforeach;?>
		   </select></td>
		   </tr>-->

		   <tr>
		   <td><h2>Cantidad</h2></td>
		   <td><?php echo form_input("quantity", $product->quantity);?></td>
		   </tr>
		   <tr>
		   <!--<td><h2>Precio</h2></td>
		   <td><?php //echo form_input("price", $product->price);?></td>
		   </tr>-->
		   <tr>
		   <td><h2>Descripcion:</h2></td>
			<td><?php echo form_textarea('description', $product->description);?></td>
			</tr>
			
		  <?php if($product->featured == 0):?>
			<tr>
			<td><h2>Destacar producto? </h2></td>
			<td><?php echo form_checkbox("featured", "1");?></td>
			</tr>
		   <?php endif;?>
		   <tr>
		   <td><h2>Material </h2></td>
		   <td><?php echo form_input("material", $product->material);?></td>
		   </tr>
		   <tr>
		   <td><h2>Color</h2></td>
		   <td><?php echo form_input("color", $product->color);?></td>
		   </tr>
		   <tr>
		   <td><h2>Acabado</h2></td>
		   <td><?php echo form_input("finished", $product->finished);?></td>
		   </tr>
		   <tr>
		   	<?php if($product->images == null):?>
		   <td><h2>Sube imagenes..</h2></td>
		   	<td>
		   	<p>
     			<?php echo form_label('Imagen 1', 'image1') ?>
     			<?php echo form_upload('image1') ?>
 			</p>
 			<p>
     			<?php echo form_label('Imagen 2', 'image2') ?>
     			<?php echo form_upload('image2') ?>
 			</p>
 			<p>
 			<?php echo form_label('Imagen 3', 'image3') ?>
     		<?php echo form_upload('image3') ?>
 			</p>
 			<p>
     		<?php echo form_label('Imagen 4', 'image4') ?>
     		<?php echo form_upload('image4') ?>
     		</p>
		   </td>
		  	<?php endif;?>
		  </tr>
		   <td><?php echo form_submit("submit", "editar");?></td>
		   </tr>  		 		   
			<?php echo form_close();?>
		</table>
	</div>
</div>