<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Email extends CI_Email {
	private $CI = NULL;
	
	function __construct() {
		$this->CI =& get_instance();
		parent::__construct();
	}
	
	function send_registration($to, $data) {		
		$body = $this->CI->load->view("emails/registration", $data, true);
		
		$this->from("hola@nouian.com", "Andrea Rincon");
		$this->to($to);
		$this->subject("Tu registro en Nouian");
		$this->set_mailtype("html");

		$this->message($body);

		$this->send();
	}
	
	function send_order($to, $data) {
		$body = $this->CI->load->view("emails/order", $data, true);
		
		$this->from("ventas@nouian.com", "Andrea Rincon");
		$this->to($to);
		$this->subject("Tienes una nueva orden en Nouian");
		$this->set_mailtype("html");
		$this->message($body);

		$this->send();
	}
	
	function send_seller_info($to, $data) {
		$body = $this->CI->load->view("emails/seller_info", $data, true);
		
		$this->from("ventas@nouian.com", "Andrea Rincon");
		$this->to($to);
		$this->subject("Tu compra en Nouian");
		$this->set_mailtype("html");
		$this->message($body);

		$this->send();
	}

	function send_question($to, $data){
		$body = $this->CI->load->view("emails/question", $data, true);
		
		$this->from("preguntas@nouian.com", "Andrea Rincon");
		$this->to($to);
		$this->subject("Recibiste una pregunta");
		$this->set_mailtype("html");
		$this->message($body);

		$this->send();

	}

	function send_answer($to, $data){
		$body = $this->CI->load->view("emails/answer", $data, true);
		$this->from("preguntas@nouian.com", "Andrea Rincon");
		$this->to($to);
		$this->subject("Han respondido tu pregunta");
		$this->set_mailtype("html");
		$this->message($body);
		$this->send();
	}
   
   function send_rate_reminder_buyer($to, $data) {
      $body = $this->CI->load->view("emails/rate_reminder_buyer", $data, true);
      
      $this->from("ventas@nouian.com", "Andrea Rincon");
      $this->to($to);
      $this->subject("Tu compra en Nouian");
      $this->set_mailtype("html");
      $this->message($body);
      
      $this->send();
   }
   
   function send_rate_reminder_seller($to, $data) {
      $body = $this->CI->load->view("emails/rate_reminder_seller", $data, true);
      
      $this->from("ventas@nouian.com", "Andrea Rincon");
      $this->to($to);
      $this->subject("Tu venta en Nouian");
      $this->set_mailtype("html");
      $this->message($body);
      
      $this->send();
   }
}