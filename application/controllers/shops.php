<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php 

class Shops extends Application{
	//var $before_filter = array("name" => "authorization", "except" => "show");

   function __construct(){
      
      parent::__construct();
      //$this->ag_auth->restrict();
      
   }
   
   
   
   //shop listin'
   public function index(){
   	$data['pagetitle'] = 'Tiendas';
      $data['shops'] = Shop::all();
      $yield = $this->load->view('shops/index.php', $data, true);      
      $this->load->view("layouts/application", array('yield' => $yield));
   }
   
   
   public function show(){
      $name = $this->uri->segment(1);   
      $data['shop'] = Shop::find_by_name_url($name);
      $data['pagetitle'] = $data['shop']->name;
      
      if (!is_null($data['shop'])){
         $yield = $this->load->view('shops/show', $data, true);
         $this->load->view("layouts/application", array('yield' => $yield));
      }
      else{
         redirect('404');
      }
   }

   public function my_shop(){
      if(username()){
         $shop = _has_shop(username());
         //var_dump(! $shop); die;
         if($shop){
            $data['shop'] = $shop;
            $data['pagetitle'] = $shop->name;
            $data['products'] = Product::find('all', array('conditions' => array('shop_id = ?',$shop->id)));
            $yield = $this->load->view("shops/my_shop.php", $data, true);
            $this->load->view("layouts/application", array('yield' => $yield));
         }
         else{
            redirect('shops/create_new');         
         }
      } else {
         redirect('admin');
      }
      
   }
   
   public function create_new(){
      $data['user'] = User::find_by_username(username());
      $data['pagetitle'] = array('Panel', 'Crear nueva tienda');
      
      if($data['user']->profile->id){
         if(! _has_shop(username() ) ) {
            $this->ag_auth->view('shops/new.php', $data);
         } else {
            redirect('shops/my_shop');            
         }
      }
      else{
         redirect('profiles/create_new');
      }
   }
   
   public function save_new(){ 
      $data['user'] = User::find_by_username(username());
      
      if($data['user']->profile->id){     
      	if(! _has_shop( username() ) && (!is_null($this->input->post())) ) {      
      		$attributes = $this->input->post();
      		unset($attributes['submit']);
      		
      		$shop = new Shop($attributes);
   		   $shop->url_shop();
   		   
            if($shop->is_valid()){
   		    $saved = $shop->save();   		    
   		    if($saved == true){
                  $user = $shop->profile->user;
                  $user->group_id = "2";
                  $user->save();
                  $this->session->set_userdata("group_id", "2");
                  
                  $path = $this->_create_file_path();
                  $this->_create_credit();
                  
                  redirect('shops/my_shop');
   		    } else {
                  $data['errors'] = $shop->errors->full_messages();
                  $data['user']   = User::find_by_username(username());
                  $data['pagetitle'] = array('Panel', 'Crear tienda');
                  
                  $this->ag_auth->view('shops/new.php', $data);
      		 }
            }
            else{
               var_dump("foo");
               $this->session->set_flashdata("name", $_POST["name"]);
               $this->session->set_flashdata("description", $this->input->post("description"));
               $this->session->set_flashdata("errors", $shop->errors->full_messages());              
               redirect("shops/create_new");               

            }
      	} else {
      		redirect('shops/my_shop');
      	}
      } else {
   	   redirect('profiles/create_new');
      }
   }
   
   public function edit_shop(){
      $data['shop'] = _has_shop(username());
      $data['pagetitle'] = array('Panel', 'Editar tienda');
      
      if($data['shop']){
         $this->ag_auth->view('shops/edit.php', $data);
      } else {
         redirect('shops/create_new');
      }
   }
   
   
   private function _create_file_path(){
      $shop = _has_shop(username());
      $path = "./assets/images/shops/$shop->id";
      mkdir($path, 0755);
      return $path;
   }
   
   private function _create_credit(){
      $shop = _has_shop(username());
      $credit = new Credit();
      $credit->shop_id = $shop->id;
      $credit->save();
   }
   
   
   public function do_upload(){
      $shop = _has_shop(username());
      $config['upload_path'] = "./assets/images/shops/$shop->id";
      $config['file_name'] = 'banner';
      $config['allowed_types'] = 'gif|jpg|png';
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload())
      {                                                            
      	$data['error'] = $this->upload->display_errors();
      	$data['shop'] = $shop;
      	$data['pagetitle'] = array('Panel', 'Editar tienda');
      	$this->ag_auth->view('shops/edit.php', $data);                 
      }                                                            
      else                                                         
      {  
         $info = $this->upload->data();         
         $shop->banner = $info['file_name'];
         $shop->save();
      	redirect('shops/my_shop');      	
      }                                                            
      
   }

}

