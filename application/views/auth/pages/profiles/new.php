<?php if ( !logged_in() ) {
       			redirect("login");
				die();
       		}
?>
<h2>Create <?php echo $tablename;?></h2>
<?php if ($this->session->flashdata("errors")!= null):?>
   <ul>
   <?php foreach ($this->session->flashdata("errors") as $error): ?>
   <li><span class = "error"><?php echo $error;?></span></li>
   <?php endforeach;?>
   </ul>
<?php endif;?>
<?php echo form_open('profiles/save_new', "name = 'frm' id = 'completenew'");?>
<?php echo form_hidden('user_id', $user->id);?>
<?php $attributes = array('class' => 'inputbox','name' => 'property_type');?>
<table>
<tr>
<td class="space"><span class="spanform">Nombre: </span> </td>
<td><?php echo form_input('first_name',$this->session->flashdata("first_name"));?></td>
</tr>
<tr>
<td class="space"><span class="spanform">Apellido: </span></td>
<td><?php echo form_input('last_name',$this->session->flashdata("last_name"));?></td>
</tr>
<tr>
<td class="space"><span class="spanform">Telefono: </span> </td>
<td><?php echo form_input('telephone' ,$this->session->flashdata("telephone"));?></td>
</tr>
<tr>
<td class="space"><span class="spanform">Codigo Postal: </span> </td>
<td><?php echo form_input('postal_code' ,$this->session->flashdata("postal_code"));?></td>
</tr>
<tr>
<td class="space"><span class="spanform">Direccion: </span></td>
<td><?php echo form_input('address',$this->session->flashdata("address"));?></td>
</tr>
<tr>
<td class="space"><span class="spanform" style = "display:none;">Pais: </span></td>
<td style = "display:none;"><?php echo form_input('country', "Mexico");?></td>
</tr>
<tr>
<td class="space"><span class="spanform">Estado:</span> </td>
<td><div class="styled-select"><select  name="state" onChange="cambia(document.frm.city)"> 
   <option value="   Aguascalientes "> Aguascalientes </option>
   <option value="   Baja California   "> Baja California   </option>
   <option value="   Baja California Sur  "> Baja California Sur  </option>
   <option value="   Campeche "> Campeche </option>
   <option value="   Coahuila "> Coahuila </option>
   <option value="   Colima   "> Colima   </option>
   <option value="   Chiapas  "> Chiapas  </option>
   <option value="   Chihuahua   "> Chihuahua   </option>
   <option value="   Distrito Federal  "> Distrito Federal  </option>
   <option value="   Durango  "> Durango  </option>
   <option value="   Guanajuato  "> Guanajuato  </option>
   <option value="   Guerrero "> Guerrero </option>
   <option value="   Hidalgo  "> Hidalgo  </option>
   <option value="   Jalisco  "> Jalisco  </option>
   <option value="   México   "> México   </option>
   <option value="   Michoacan   "> Michoacan   </option>
   <option value="   Morelos  "> Morelos  </option>
   <option value="   Nayarit  "> Nayarit  </option>
   <option value="   Nuevo León  "> Nuevo León  </option>
   <option value="   Oaxaca   "> Oaxaca   </option>
   <option value="   Puebla   "> Puebla   </option>
   <option value="   Querétaro   "> Querétaro   </option>
   <option value="   Quintana Roo   "> Quintana Roo   </option>
   <option value="   San Luis Potosí   "> San Luis Potosí   </option>
   <option value="   Sinaloa  "> Sinaloa  </option>
   <option value="   Sonora   "> Sonora   </option>
   <option value="   Tabasco  "> Tabasco  </option>
   <option value="   Tamaulipas  "> Tamaulipas  </option>
   <option value="   Tlaxcala "> Tlaxcala </option>
   <option value="   Veracruz "> Veracruz </option>
   <option value="   Yucatán  "> Yucatán  </option>
   <option value="   Zacatecas   "> Zacatecas   </option>
   </select></div></td>
</tr>
<tr>
<td class="space"><span class="spanform">Municipio</span></td>
<td><div class="styled-select">
   <select name="city" ><!--onChange="location.href=this.form.state.options[this.form.state.selectedIndex].value"> -->
   <option value="0">Selecciona una</option> 
   </select>
   </div>
</td>
</tr>
<tr>
<td></td>
<td style="text-align:right">
<?php echo form_submit('submit','enviar');?></td>
</tr>
</table>

 
 
<?php echo form_close();?>
