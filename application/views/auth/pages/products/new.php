<div id="upload-form">
	<div id="upload-form-content">

      <h1> Crea un nuevo producto</h1>
		<h2>Paso 1 ingresa la informacion de tu producto</h2>
		  <?php if ($this->session->flashdata("errors")!= null):?>
		   
			 <ul>
			 <?php foreach($this->session->flashdata("errors") as $error): ?>
			 <li><span class = "error"><?php echo $error;?></span></li>
			 <?php endforeach;?>
			 </ul>
		  <?php endif;?>
		<table>
			 <?php echo form_open_multipart('products/save_new');?>
			 <tr>
			 <td><h2>Elige la categoria..</h2></td>
			 <td><div class="styled-select"><select name = "category_id" >
				<?php foreach($categories as $category):?>
				   <option value ="<?php echo $category->id;?>"><?php echo $category->name;?></option>
				<?php endforeach;?>
			 </select></div>
			 <td>
			   </tr>
			<tr>
			<td><h2>Nombre del producto</h2></td>
			<td><?php echo form_input("name",$this->session->flashdata("name"));?></td>			

			<!--<td><input type="input" name="name" value="<?php echo set_value();?>" /></td>-->
			</tr>
			<tr>
			<td><h2>Cantidad</h2></td>
			<td> <?php echo form_input("quantity",$this->session->flashdata("quantity"));?></td>
			</tr>
			<tr>
			<td><h2>Precio </h2></td>
			<td> <?php echo form_input("price",$this->session->flashdata("price"));?></td>
			</tr>
			<tr>
			<td><h2>Descripcion:</h2></td>
			<td><?php echo form_textarea('description',$this->session->flashdata("description"));?></td>
			</tr>
			<tr>
			<td><span id="destacar">Destacar producto?</span></td>
			<td><?php echo form_checkbox("featured", "1");?></td>
			</tr>
			<tr>
			<td><h2>Opciones de pago:</h2></td>
			</tr>
			<tr>
			<td><h3>Deposito bancario</h3></td>
			<td><?php echo form_checkbox("payment1", "deposito bancario");?></td>
			</tr>
			<tr>
			<td><h3>A convenir con el cliente</h3></td>
			<td><?php echo form_checkbox("payment2", "a convenir");?></td>
			</tr>
			<tr>
			<td><h2>Opciones de envio</h2> </td>
			</tr>
			<tr>
			<td><h3>Contra entrega</h3></td>
			<td><?php echo form_checkbox("delivery1", "contra entrega");?></td>
			</tr>
			<tr>
			<td> <h3>(Fedex, multipack, dhl, etc..)</h3></td>
			<td> <?php echo form_checkbox("delivery2", "paqueteria");?></td>
			</tr>
			<tr>
			<td><h2>Material </h2></td>
			<td><?php echo form_input("material",$this->session->flashdata("material"));?></td>
			</tr>
			<tr>
			<td><h2>Color</h2></td>
			<td><?php echo form_input("color",$this->session->flashdata("color"));?></td>
			</tr>
			<tr>
			<td><h2>Acabado</h2></td>
			<td><?php echo form_input("finished",$this->session->flashdata("finished"));?></td>
			</tr>
			<tr>
			<td></td>
			<td><?php echo form_submit("submit", "crear");?></td>
			</tr>
			 <?php echo form_close();?>
		</table> 
         
      </div>
</div>