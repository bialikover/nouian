<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php echo ( isset($pagetitle) ) ? pagetitle($pagetitle) : pagetitle(); ?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/js/libs/fancy/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/js/libs/alert/jquery.alerts.css">
	<link rel="shorcut icon" href="../assets/images/favicon.ico">
	<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/vnd.microsoft.icon" />
	<link rel="icon" href="../assets/images/favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/nouian.css">
  <script src="<?php echo base_url();?>assets/js/libs/modernizr-2.5.3.min.js"></script>
	<!--Inician Facebook OG Headers=-->
	<?php if (isset($product)): ?>
	<meta property="og:title" content="<?php echo $product->name;?>" />
	<meta property="og:description" content="<?php echo $product->description;?>" />
	<meta property="og:url" content="<?php echo base_url().$product->name_url; ?>" />
	<meta property="og:locale" content="es_MX" />
	<meta property="og:site_name" content="Nouian" />
	<meta property="og:image" content="<?php echo base_url()."images/product/370x370".$product->images[0]->image_url; ?>" />
	<?php elseif (isset($shop)): ?>
	<meta property="og:title" content="<?php echo $shop->name;?>" />
	<meta property="og:description" content="<?php echo $shop->description;?>" />
	<meta property="og:url" content="<?php echo base_url().$shop->name_url; ?>" />
	<meta property="og:locale" content="es_MX" />
	<meta property="og:site_name" content="Nouian" />
	<?php endif; ?>
	<!--Terminan Facebook OG Headers=-->
</head>
<body>
   <!-- FB SDK w/ XFBML -->
   <div id="fb-root"></div>
   <script>(function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) return;
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=226437797487663";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));</script>
   
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
  <header>
        <div id="header">
			<div class="headsection">
			<?php echo anchor(base_url(), " <div id='logo'><h1><span>Nouian</span></h1></div>")?>
			</div>
			<div class="headsection">
			<div id="menu">
				<div id="actions">
					<nav> 
						<ul>
							<li><?php echo anchor("products/create_new","Vende");?></li>
							<li><?php echo anchor("shops","Descubre");?></li>
						</ul>
					</nav>
				</div>
			</div>
			</div>
			<div class="headsection">
			<div id="user"> 
			<?php if(username()):?>
			   <span id="usernamehead"><?php echo anchor("admin/dashboard", username());?></span>
			   <span id="closession"><?php echo anchor('logout', 'Cerrar sesi&oacute;n'); ?></span>
			<?php else:?>
			   <span id="signinhead"><?php echo anchor("/login", "Conectate");?>
			<?php endif;?>
			</div>
			</div>
			</div> <!--Termina menu=-->
      </div><!--Termina header=-->
  </header>
  <div role="main">
     <div id="content">
        <?php echo $yield;?>
     </div>
  </div>
  <footer>
     	<div id="footer">
			<div id="menufooter">
			<ul>
						
						
						<li><a href="<?php echo base_url();?>faq/ayuda">Ayuda</a></li>
						<li><a href="<?php echo base_url();?>faq/terminos">Terminos</a></li>
			</ul>
			</div><span id="spanfoot" >Nouian © 2012</span>
			
			</div> <!--end footer=-->
  </footer>


  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/libs/fancy/jquery.fancybox.pack.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/libs/alert/jquery.alerts.js"></script>
  <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
  <script src="<?php echo base_url();?>assets/js/nouian.js"></script>
  <script type="text/javascript">
	 jQuery(document).ready(function(){
		$("a#single_image").fancybox();
	});
  </script>
  <script>
    var _gaq=[['_setAccount','UA-30778602-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
  <!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
	$.src='//cdn.zopim.com/?4Azpgkipqjz7qPBftOq3DZ6MjPbvrWeT';z.t=+new Date;$.
	type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
	</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>

