		<h1> Mi Resumen </h1>
		<?php if(! _has_profile(get_user(username())) ): ?>
	      <div class="alert">
		  <h4>Atención!</h4>
		  <p>Parece que no has completed del todo tu registro…</p>
      	<?php echo anchor("profiles/create_new", "completar");?>
		</div>
      <?php endif;?>
		<?php if( $shop = _has_shop(username())):?>
		<?php if($shop->credit->ammount < 3):?>
		<div id="importante">
		<div id="importante-content">

			<h1> ¡Importante!</h1>
			<ul>
			<li>Tu credito Nouian esta por agotarse <a>¡Compra ahora y te regalamos el doble!</a></li>
			<li>Antes de vender te sugerimos leer esta <a>guia.</a></li>
			<li>Comienza a vender ahora y obten <a>GRATIS $100 pesos de credito Nouian.</a></li>
			</ul>
		</div>
		</div>
        <?php endif;?>
		<br/>
			<ul>
			   <li>Credito Nouian: <span id="nouian-highlight">$<?php echo $shop->credit->ammount; ?></span></li>
		   </ul>
			<?php if ( isset($sales) ) : ?>
			   <?php if ( $sales -> total_sales > 0 ) : ?>
   			   <h2>Resumen de mis ventas</h2>
   		      <ul>
      			   <li>Ventas del mes: <span id="nouian-highlight">$<?php echo $sales->total_sales; ?></span></li>
      			   <li>Has vendido <?php echo $sales->products_sold; ?> articulos en el mes</li>
      			   <?php if ( $sales->ratings_pending > 0 ) : ?>
      			      <li>Tienes <?php echo $sales->ratings_pending; ?> ventas por calificar</li>
      			   <?php endif; ?>
   			   </ul>
			   <?php endif; ?>
			<?php endif; ?>
       <?php endif;?>
		<?php /*
		<h2> Mis Compras Resumen</h2>
			<ul>
			<li>Tienes 2 compras por calificar</li>
			<li></li>
			</ul>
		<h2> Mis Preguntas Resumen</h2>
			<ul>
			<li>Han respondido 3 de tus preguntas</li>
			<li>Tienes 4 nuevas preguntas ¡Respondelas!</li>
			<li></li>
			</ul> */ ?>