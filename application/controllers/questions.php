<?php 

class Questions extends Application{
   function __construct(){      
      parent::__construct();
      //$this->ag_auth->restrict();
//foo      
   }
  
  
 /**
 * send a comment
 *
 * @return .json with status and message
 * @author adanowsky
 */
 
  public function i_made(){
     $user = User::find_by_username(username());
     if(_has_profile($user)){
     	  $data['pagetitle'] = array('Panel', 'Preguntas que he hecho');
        $data['questions'] = Question::find_all_by_profile_id($user->profile->id);
        $this->ag_auth->view('questions/show', $data);
     }
     else{
          redirect('profiles/create_new');
     }
  }
  
  public function made_to_me(){
       $user = User::find_by_username(username());
       if(_has_profile($user)){
       	$data['pagetitle'] = array('Panel', 'Preguntas que me han hecho');
       	$data['questions'] = Question::find(
       		"all",
       		array(
       			'conditions' => array(
       				"seller_id = (?) AND answer IS NULL",
       				$user->profile->id
       			)
       		)
       	);
       	$this->ag_auth->view('questions/reply', $data);
       } else {
          redirect('profiles/create_new');
       }
  }
  
  
  public function update(){
     $attributes['answer']      = $this -> input -> post('answer');
     $attributes['answered_at'] = date("Ymd H:i:s");
     
     $question = Question::find($this -> input -> post('id'));
     $saved    = $question -> update_attributes($attributes);
     $email    = $question -> profile -> user -> email;
     
     if ( $saved == true ){
        $data['buyer']   = $question -> profile -> user -> profile;
        $data['product'] = $question -> product;
        
        $this->email->send_answer($email, $data);
        redirect("producto/".$question -> product -> name_url);  
     } else {
     	 $data['pagetitle'] = 'Nueva pregunta';
     	 $data['errors']    = $question -> errors -> full_messages();
       $this -> ag_auth -> view('questions/reply', $data);
     }
  }
 
  public function save(){
     $user = User::find_by_username(username());
     
     if( _has_profile($user) ){
        $attributes = $this->input->post();
        unset($attributes['submit']);
        
        $product = Product::find($attributes['product_id']);
        $seller = $product->shop->profile->user->email;
        
        $attributes['profile_id'] = $user->profile->id;
        $attributes['seller_id']  = $product->shop->profile->id;
        $attributes['made_at']    = date("Ymd H:i:s");
          
        $question = new Question($attributes);     
        $saved = $question->save(false);
         
        if($saved == true){
          $data['seller']  = $product->shop;
          $data['product'] = $product->name;
          
          $this->email->send_question($seller ,$data);
         redirect("producto/".$question->product->name_url);
        } else{
           redirect("producto/".$question->product->name_url);
        }
     } else{
         redirect('profiles/create_new');
     }
  }
}

