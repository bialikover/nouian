<?php

class Admin extends Application {
   public function __construct() {
      parent::__construct();
   }

   public function index() {
      if (logged_in()) {
         $data['user']       = User::find_by_username(username());
         $data['profile']    = $data['user'] -> profile;
         $data['reputation'] = null;
         $data['pagetitle']  = array('Panel', 'Calificar');

         if ($data['profile']) {
            $data['reputation']['as_buyer']  = $data['profile'] -> overall_buyer;
            $data['reputation']['as_seller'] = $data['profile'] -> overall_seller;
            
            if ( $data['user'] -> group_id = '2' )
               $data['sales'] = Order::monthly_sales($data['profile'] -> id);
         }
         
         $this -> ag_auth -> view('dashboard', $data);
      } else {
         $this -> login('admin/dashboard');
      }
   }

	public function entrar(){
		
		$this->login($this->session->userdata("redirect"));
	}

}

/* End of file: dashboard.php */
/* Location: application/controllers/admin/dashboard.php */
