<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php echo ( isset($pagetitle) ) ? pagetitle($pagetitle) : pagetitle(); ?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/nouian.css">
  <link rel="shorcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/vnd.microsoft.icon" />
	<link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/vnd.microsoft.icon" /> 
  <script src="<?php echo base_url();?>assets/js/libs/modernizr-2.5.3.min.js"></script>
</head>
<body>
   <!-- FB SDK w/ XFBML -->
   <div id="fb-root"></div>
   <script>(function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) return;
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=226437797487663";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));</script>
   
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
  <header>
		<div id="headerwrapperc">
        <div id="header">
			<div class="headsection">
			<?php echo anchor(base_url(), " <div id='logo'><h1><span>Nouian</span></h1></div>")?>
			
			</div>
			<div class="headsection">
			<div id="menu">
			<div id="actions">
			<nav> 
				<ul>
					<li><?php echo anchor("products/create_new","Vende");?></li>
					<li><?php echo anchor("shops","Descubre");?></li>
				</ul>
			</nav>
			</div> 
			</div> <!--Termina menu=-->
			
			</div>
			<div class="headsection">
			<div id="user"> 
			<?php if(username()):?>
			   <span id="usernamehead"><?php echo anchor("admin/dashboard", username());?></span>
			   <span id="closession"><?php echo anchor('logout', 'Cerrar sesi&oacute;n'); ?></span>
			<?php else:?>
			   <span id="signinhead"><?php echo anchor("/login", "Conectate");?>
			<?php endif;?>
			   
			</div>
			</div>
		</div> 
		</div> <!--Termina header=-->
  </header>
  <div role="main">
     <div id="content">
        <div id="contenido">	
         
		<div id="nav_bg">
			<?php #$this->load->view($this->config->item('auth_views_root') . 'nav'); ?>
		</div>
		
		<div id="container">