<?php

class Rate extends ActiveRecord\Model
{
   
   
   /**
    * Increases or decreases the points of a user based on the rate
    *
    * @param object $profile; the profile to increase or decrease points
    * @param string $rate_value; value of the rate. Can be: positive, negative or neutral
    * @param string $type;  the type of the profile given. Can be: buyer or seller
    * @return void
    * @author adanowsky
    */
   public function points($profile, $rate_value, $type){
      $num_sales = $profile->total_sales;
      $num_boughts = $profile->total_boughts;
      if($type == "seller"){
         if($rate_value == "negative" ){         
            switch($num_sales){
               case 0 :         
               case 1 :
               case 2 :
                  $profile->overall_seller -= 50;
                  break;
               case 3 :
                  $profile->overall_seller -= 25;
                  break;
               case 4 :
                  $profile->overall_seller -= 15;
                  break;
               case 5 :
                  $profile->overall_seller -= 5;
                  break;
               default:
                  $profile->overall_seller -= 5;
                  break;
            }
            $profile->negative_as_seller += 1;
         } 
         //if is positive...
         else if ($rate_value == "positive"){
            if($profile->overall_seller < 100){
               $profile->overall_seller += 5;
            }
            $profile->positive_as_seller += 1;
         }
         else{
            //do nothin' if neutral
         }                  
      }
      
      //if is buyer:
      if($type == "buyer"){
         if($rate_value == "negative" ){         
            switch($num_boughts){
               case 0 :         
               case 1 :
               case 2 :
                  $profile->overall_buyer -= 50;
                  break;
               case 3 :
                  $profile->overall_buyer -= 25;
                  break;
               case 4 :
                  $profile->overall_buyer -= 15;
                  break;
               case 5 :
                  $profile->overall_buyer -= 5;
                  break;
               default:
                  $profile->overall_seller -= 5;
                  break;                                 
            }
            $profile->negative_as_buyer += 1;
         } 
         //if is positive...
         else if ($rate_value == "positive"){
            if($profile->overall_buyer < 100){
               $profile->overall_buyer += 5;
            }
            $profile->positive_as_buyer += 1;
         }
         else{
            //do nothin' if neutral
         }                  
      }
      $profile->save();
//      var_dump( $profile);      
   }
}