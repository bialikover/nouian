<?php

class Product extends ActiveRecord\Model
{

   static $before_validation = array('url_product');
   static $after_create = array('decrease_credit');
//   static $after_update = array('decrease_credit');
//   static $after_update = array('verify_quantity_and_featured');
   static $validates_presence_of = array(
         array('shop_id'),
         array('name', 'message' => 'debe de tener minimo 10 letras y maximo 38'),
         array('quantity',  'message' => 'debes de especificar una cantidad'),
         array('price',  'message' => 'debes de ponerle un precio')
   );
   static $validates_size_of = array(
     array('name', 'minimum' => 10, 'too_short' => 'el nombre es muy corto, minimo 10 letras!'),
     array('name', 'maximum' => 38, 'too_long' => 'Esta muy largo, deben de ser 38 letras maximo'),
     array('description', 'minimum' => 30, 'too_short' => 'Esta muy corta minimo 30 letras!'),
     array('description', 'maximum' => 538, 'too_long' => 'Esta muy larga son maximo 538 letras')
   );
   
   
   
   static $validates_numericality_of = array(
       array('price', 'greater_than' => 5, 'less_than'=>99999,  'message' => 'debe ser mayor a 5 y menor que 99999'),
       array('quantity', 'only_integer' => true, 'greater_than' => 0, 'less_than'=>100,  'message' => 'debe ser mayor que 0 y mnor a 100 piezas, paquetes, etc')
      );

   static $validates_uniqueness_of = array(
      array('name_url',  'message' => 'El nombre debe ser unico')
      //array(, 'message' => 'Ese nombre ya existe')
    );


   static $has_many = array(
      array('images'),
      array('questions')
   );
   static $belongs_to = array(
   	array('shop'),
   	array('order'),
   	array('category')
   );
   
   /**
    * Decreases the shop's credit
    *
    * @return void
    * @author bialikover
    **/
   public function decrease_credit()
   {
       $shop = _has_shop(username());
       $credit = $shop->credit;
       $credit->ammount = $credit->ammount - 3;
       $credit->save();      
   }
   
   
   public function set_attributes(){
      $op = array();
      $payment = $this->input->post('payment1');
      $payment2 = $this->input->post('payment2');
      $payment_options = $payment.", ".$payment2;
      if($payment_options != ", "){
         $op['payment_opt'] = $payment_options;          
      }
      else{
         $op['payment_opt'] = "a convenir";
      }
      $delivery = $this->input->post('delivery1');
      $delivery2 = $this->input->post('delivery2');
      $delivery_options = $delivery.", ".$delivery2;
      if($delivery_options != ", "){
         $op['shipping_opt'] = $delivery_options;
      }
      else{
         $op['shipping_opt'] = "contra entrega";
      }
      
      return $op;
   }

    public function url_product(){

    //$this->name_url = strtolower( preg_replace("[^A-Za-z0-9]", "", $this->name));
     //$this->name_url = preg_replace("![^a-z0-9]+!i", "-", $name);
      $this->name_url = strtolower( preg_replace("![^a-z0-9]+!i", "-", $this->name));
    //$this->name_url = $this->validate_name_url($name_url);
    //htmlspecialchars(str_replace(" ", "", strtolower($this->name)),ENT_QUOTES);

   }

/*
    public function validate_name_url($name_url){
      $product = Product::find_by_name_url($name_url);      
      if( !is_null($product)){
        $name_number = substr($product->name_url, -1);
        //$name_number = count($products);
        //var_dump($name_number);
        //var_dump($product->name_url);
        //die;
        if(is_numeric($name_number)){

          $last = $name_number+1;
          //$name_url = substr( $name_url, 0, -1);
          return $name_url.$last;
        }
        else{
          return $name_url."1";
        }
      }
      else{
        return $name_url;
      }

    }*/
   
   
}