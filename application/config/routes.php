<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['scaffolding_trigger'] = 'scaffolding';
|
| This route lets you set a "secret" word that will trigger the
| scaffolding feature for added security. Note: Scaffolding must be
| enabled in the controller in which you intend to use it.   The reserved 
| routes must come before any wildcard or regular expression routes.
|
*/


// controllers
if($handle = opendir(APPPATH.'/controllers'))
{
	while(false !== ($controller = readdir($handle)))
	{
		if($controller != '.' && $controller != '..' && strstr($controller, '.') == '.php')
		{
			$controllers[] = strstr($controller, '.', true);
		}
	}
closedir($handle);
}

$url_parts = explode('/',$_SERVER['REQUEST_URI']);
$reserved_routes = $controllers;
$reserved_routes[] = "productos";
$reserved_routes[] = "categoria";
$reserved_routes[] = "categorias";
$reserved_routes[] = "404";

// these are the /username routes
//cambia deacuerdo al path es 2 por que es: localhost:8888/nouian/products..
//												[0], 	   [1]    [2]
$env = ENVIRONMENT;


if($env == 'production' || $env == 'testing'){



	if( ! in_array($url_parts[1], $reserved_routes))
	{

		//echo var_dump($url_parts);
		//die;
		$route['([a-zA-Z0-9_-]+)'] = 'shops/show/$1';
		//$route['([a-zA-Z0-9_-]+)/subpage'] = 'shops/subpage/$1';
		$route['producto/([a-zA-Z0-9_-]+)'] = 'products/show/$1';
	}
}

else{
if( ! in_array($url_parts[2], $reserved_routes))
	{

		//echo var_dump($url_parts);
		//die;
		$route['([a-zA-Z0-9_-]+)'] = 'shops/show/$1';
		//$route['([a-zA-Z0-9_-]+)/subpage'] = 'shops/subpage/$1';
		$route['producto/([a-zA-Z0-9_-]+)'] = 'products/show/$1';
	}


}

//$route['products/show/$1'] = 

$route['404_override'] = 'nouian404';
$route['admin'] = "admin/admin/index";
$route['default_controller'] = "welcome";
$route['welcome/show/(:any)']= "products/show";
//$route['shops/([a-zA-Z0-9_-]+)']= "shops/show/$1";
//$route['shops/show/(:any)']= "shops/show";
$route['productos']= "products/index";
$route['producto/(:any)']= "products/show/$1";
$route['producto/crear_nuevo'] = "products/create_new";
$route['producto'] = "products";



$route['tiendas']= "shops/index";
$route['tienda/(:any)']= "shops/show/$1";
$route['tienda']= "shops";


$route['perfiles'] = "shops";
$route['perfil/(:any)'] = "shops/show/$1";

$route['categorias'] = "welcome/categories";
$route['categoria/(:any)']= "welcome/category";

$route['admin/orders/rate/(:any)'] = "admin/rates/show";
$route['admin/rate/order'] = "admin/rates/rate";
$route['faq/credito'] = "pages/credit";
$route['faq'] = "pages/faq";
$route['faq/ayuda'] = "pages/ayuda";
$route['faq/terminos'] = "pages/terminos";
$route['blog'] = "pages/blog";
$route['faq/acerca'] = "pages/acerca";
$route['contacto'] = "pages/contacto";
//$route['scaffolding_trigger'] = "";


// BEGIN AUTHENTICATION LIBRARY ROUTES
$route['login'] = "admin/admin/entrar";
$route['logout'] = "admin/admin/logout";
$route['register'] = "admin/admin/register";
$route['admin/dashboard'] = "admin/admin/index";

$route['imagenes'] = 'images';
//$route['dashboard']
// END AUTHENTICATION LIBRARY ROUTES

/* End of file routes.php */
/* Location: ./system/application/config/routes.php */