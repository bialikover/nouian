<div id="ayuda-nouian">
<h1>FAQ</h1>
<h2><strong>Acerca de nouian</strong><a name="About"></a></h2>
  <h3>&iquest;Qu&eacute; es nouian?</h3>
  <p>nouian es una plataforma de compra-venta de creaciones independientes, dise&ntilde;o, arte y artesan&iacute;a en habla hispana.</p>
  <h3>&iquest;Cu&aacute;l es su objetivo?</h3>
  <p>nouian est&aacute; especialmente pensado para que los compradores se relacionen con creadores independientes y encuentren productos originales y con identidad propia. Asimismo, provee a dise&ntilde;adores, artistas y artesanos de un espacio excepcional en el que construir o expandir su negocio.</p>
  <h3>&iquest;Qu&eacute; hace de nouian un sitio especial?</h3>
  <p>nouian es especial porque se sustenta en una comunidad din&aacute;mica de nouiannos que valoran la identidad y originalidad de cada producto, eligiendo apoyar y desarrollar la creatividad independiente. </p>
  <h3>&iquest;A qui&eacute;nes est&aacute; destinado?</h3>
  <p>Est&aacute; destinado a quienes quieran informarse, comprar, vender e interactuar dentro del mundo del dise&ntilde;o y la artesan&iacute;a.</p>
  <h3>&iquest;Qu&eacute; ventajas ofrece nouian?</h3>
  <ul>
    <li>Propone nuevos beneficios y posicionamiento estrat&eacute;gico de alto potencial a muy bajo costo.</li>
    <li>Es simple, c&oacute;modo y con tecnolog&iacute;a de alt&iacute;simo desarrollo para garantizar navegabilidad, seguridad y calidad est&eacute;tica.</li>
    <li>De f&aacute;cil acceso, est&aacute; pensado para que los vendedores creen o expandan su negocio r&aacute;pidamente en el &aacute;mbito nacional e internacional.</li>
  </ul>
  <p>Un beneficio importante para dar los primeros pasos: <strong>&iexcl;publicar es gratis!</strong></p>
  <h3>Conoc&eacute; a nouian en profundidad</h3>
  <ul>
    <li>Como vendedor, cuanto m&aacute;s conozcas c&oacute;mo funciona nouian, mejor preparado estar&aacute;s para ayudar a tus clientes y darles un mejor servicio.</li>
    <li>Para poder vender es necesario entender c&oacute;mo se comportan los compradores. Conocer su conducta de compra ayudar&aacute; a la hora de definir aspectos relacionados con las ventas.</li>
  </ul>
  <p>Para hacer la mejor de las compras es aconsejable conocer c&oacute;mo funciona el sitio.</p>
  <h2>Costos<a name="costos"></a></h2>
  <h3>&iquest;Cu&aacute;nto cuesta ser usuario en nouian?</h3>
  <p>Ser usuario de nouian es totalmente gratuito. S&oacute;lo tienes que <a href="<?php echo base_url();?>/products/create_new/">registrarte</a>. Es simple y seguro. <a href="<?php echo base_url();?>/products/create_new/">&iexcl;No te quedes afuera!</a></p>
  <h3>&iquest;Cu&aacute;nto cuesta publicar en nouian?</h3>
  <p>&iexcl;Publicar en nouian es totalmente gratuito! nouian est&aacute; especialmente pensado para que dise&ntilde;adores y artesanos puedan desarrollar su negocio independiente. Por eso, la publicaci&oacute;n gratuita es una ayuda para dar los primeros pasos.</p>
  <h3>&iquest;Cu&aacute;nto cuesta vender en nouian?</h3>
  <p>nouian cobra 5% de comisi&oacute;n sobre el precio del producto por cada transacci&oacute;n concretada. Si el producto no fue vendido, no se cobrar&aacute; comisi&oacute;n.</p>
  <h3>&iquest;Qu&eacute; son los espacios destacados en nouian?</h3>
  <p>Utilizar los destacados es una forma genial de atraer compradores a la tienda. Adquiriendo un espacio destacado se est&aacute; obteniendo un m&eacute;todo de publicidad enfocado directamente en los potenciales compradores de tu producto. </p>
  <h2>Vender en nouian <a name="vender"></a></h2>
  <h3>Para tener en cuenta:</h3>
  <ul>
    <li>nouian es un sitio exclusivo para dise&ntilde;o, arte y artesan&iacute;a. S&oacute;lo se pueden publicar productos relacionados con estas &aacute;reas, pensados y desarrollados creativamente, con originalidad e identidad propia.</li>
    <li>Utilizar los destacados es una forma genial de atraer compradores a la tienda y aumentar hasta cinco veces las posibilidades de venta. Adquiriendo un espacio destacado se est&aacute; obteniendo un m&eacute;todo de publicidad enfocado directamente en los potenciales compradores de tu producto.</li>
    <li>Formar parte de nouian y su comunidad significa tener infinitas oportunidades para descubrir personas interesantes, expandirse comercialmente y crecer como creador.</li>
  </ul>
  <h3>&iquest;Qu&eacute; es una tienda dentro de nouian?</h3>
  <p>nouian te permite crear tu propia tienda personalizada dentro del sitio. All&iacute; pod&eacute;s publicar tus productos totalmente gratis en pocos pasos simples y completos. Adem&aacute;s, tu tienda es el espacio que la comunidad nouianna tiene como herramienta para conocerte, no s&oacute;lo por lo que hac&eacute;s, sino tambi&eacute;n por lo que te gusta (productos y tiendas de nouian favoritos), lo que dec&iacute;s sobre vos (perfil) y lo que hac&eacute;s fuera del sitio (facebook, blog, twitter...). </p>
  <h3>&iquest;Qu&eacute; ventajas tiene tener una tienda?</h3>
  <p>Tener una tienda te permite no s&oacute;lo vender tus productos a todo el mundo, sino tambi&eacute;n dar a conocer tu marca, integrar el contenido de tu tienda con las redes sociales y tener un nuevo canal comercial de alto impacto. </p>
  <h3>&iquest;C&oacute;mo hago para tener mi propia tienda en nouian?</h3>
  <p>Para tener tu propia tienda en nouian s&oacute;lo tienes que <a href="<?php echo base_url();?>register/">registrarte como vendedor</a> y crearte una cuenta en <a href="https://www.mercadopago.com/mla/registration">MercadoPago</a> para poder cobrar con todos los medios de pago de la forma m&aacute;s simple y segura.</p>
  <p><strong>Si tienes alguna pregunta o comentario, &iexcl;no dudes en <a href="<?php echo base_url();?>contacto/" style="text-decoration:underline">contactarnos</a>!</strong></p>
  
    </div>