<h1> Mis Preguntas</h1>

<div id="questions">
    <?php if(! empty($questions)):?>
    <?php foreach ($questions as $question):?>
      <div id = "questionitem">
	    <div id="questionsimage">
	  
      <?php if ( ! is_null($question->product->images[0]->image_url)):?>
        <?php echo anchor("producto/".$question->product->name_url, 
            '<img id="productpanelimg" width = "100" height = "100" src = "'.base_url() ."images/product/100x100". $question->product->images[0]->image_url.'"/>'
         );?>
      <?php else:?>
	      <?php echo anchor("producto/".$question->product->name_url, 
	        "<img src='".base_url()."/assets/images/noimage".rand(0,4).".png'>" );?>
	    <?php endif;?>
	    </div>
	    
	    <div id="questionsproducttitle">
		    <h4><?php echo anchor("producto/".$question->product->name_url, $question->product->name );?></h4>
	    </div>
	    <div id="questionsproduct">
		    <ul id="questionsanswered">
			    <li id="questionpanel">Pregunta: <?php echo $question->content;?>
			      <span class="timepanel"><?php echo RelativeTime($question->made_at);?></span>
			    </li>
			    <?php if($question->answer != null):?>
			    <li id="answerpanel">Respuesta: <?php echo $question->answer; ?>
			        <span class="timepanel"><?php echo RelativeTime($question->answered_at);?></span>
			    </li>
			    <?php endif;?>
		    </ul>
		
		  </div>
		</div>
    <?php endforeach;?>
	<?php else:?>
	<h3>Aun no has hecho preguntas..<h3>
    <?php endif;?>
</div>
