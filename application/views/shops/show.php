<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<div id="tienda-header">
		<div id="tienda-izq">
		   <?php if($shop->banner!= null):?>
   		   <img src="<?php echo  base_url()."images/shop/370x370/".$shop->id."/".$shop->banner;?>" alt="<?php echo $shop->name;?>"/>
   		<?php else:?>
   		   <img src="<?php echo  base_url().'assets/images/noimage'.rand(0,4).'.png'?>" alt="<?php echo $shop->name;?>"/>
   		<?php endif;?>
		</div>
		<div id="tienda-der">
			<div id="descripcion-tienda">
				<!--<h1><?php echo $shop->name;?></h1>
				<p><?php echo $shop->description;?></p>
				<div id="nombre-vendedor">	
				<h2><?php echo $shop->profile->user->username;?></h2>-->
				<div id="nombre-vendedor">	
				  <div style="position: relative; float: left; margin-right: 12px; margin-top:6px; width: 70px; height: 65px;">
				     <div class="fb-like" data-send="false" data-layout="box_count" data-width="450" data-show-faces="true"></div>
              </div>
				<h2><?php echo $shop->name;?></h2> 
				<span id="triangle"></span>
				</div>
				<div id="descripcion-vendedor"><br>
					<div id="descripcion-vend-contenedor">
					<!--<img src="<?php echo get_gravatar($shop->profile->user->email);?>"></img>-->
					<p> <?php echo $shop->description;?> </p>
					</div>
				</div>
			</div>
		</div>
	</div>
   
	<div id="explore-shops">
	   <?php if( count($shop->products) !== 0):?>
         <?php foreach($shop->products as $product):?>
			<div id="cont-shop">
				<a href="<?php echo base_url()."producto/".$product->name_url?>">
				<div id="cont-imagen">
				   <?php if($product->images!= null):?>
				      <img src="<?php echo base_url(). "images/product/370x370".$product->images[0]->image_url;?>"/>
				   <?php else:?>
				      <img src="<?php echo base_url(). 'assets/images/noimage'.rand(0,4).'.png';?>"/>
				   <?php endif;?>
				</div>
				<div id="spacebck">
				<div id="shop-text">
				<span id="shop-title"><?php echo $product->name;?> </span>
				<span id="shop-title">$<?php echo $product->price;?></span><br>
				<span id="short-descript"><?php echo substr($product->description, 0, 32)."...";?></span>
				<span class="botonshop"> > </span>   
				</div>
				</div>
				</a>
			</div>
		<?php endforeach;?>
		<?php else:?>
		   <h2> Esta tienda aun no tiene productos </h2>
		<?php endif;?>
	</div><!--Termina explore-shop=-->
	<div class="clear"></div>