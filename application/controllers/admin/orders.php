<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends Application {
   
   function __construct(){
      parent::__construct();
      //$this->ag_auth->restrict();

   }

   /**
    * creates a new Order! how does it feeel?!!
    *
    * @return json: to redirect to the correct place, if it is not logged in, 
    *         if it does not have profile, or creates a new order if it has profile...
    * @author adanowsky
    **/
   
   public function my_orders(){
      //set the pagination ...
      $this->load->library('pagination');
      $this->load->library('table');
      $this->table->set_heading('#orden', 'Producto', 'Tienda', 'cantidad', 'total', 'fecha'); 
      $user = User::find_by_username(username());
      if( ! _has_profile($user)){
         redirect('profiles/create_new');
      }
      else{
      
      $profile_id = $user->profile->id;
      $tablename = "Orders";
      $orders_uncompleted = Order::find('all', 
                                    array(
                                       'conditions' => array(
                                                         'profile_id = ? AND completed IS NULL ', $profile_id),
                                                         'limit' => 10,
                                                         'offset' => $this->uri->segment(4)
                                       ));
                                       
     
      //needs eaggy loading or sth like that in order to retrive only once the product name from product_id
     foreach ($orders_uncompleted as $uncompleted){
        $actions = anchor("admin/orders/rate/".$uncompleted->id."/", "Calificar", "class='boton'"); // Build actions links
        //$actions = '<div class="rate" id = "'.$uncompleted->id .'">calificar</div>';
        $this->table->add_row(
           $uncompleted->id,
           $uncompleted->product->name,
           $uncompleted->product->shop->name,
           $uncompleted->quantity,
           $uncompleted->total,
           $uncompleted->date,
           $actions); // Adding row to table
     }
     //sets the rest of pagination...
      $config['base_url'] = site_url().'/admin/orders/my_orders';      
      $config['per_page'] = 5;
      $config['total_rows'] = count($orders_uncompleted);      
      $config['full_tag_open'] = '<div class = "pagination">';
      $config['full_tag_close'] = '</div>';      
      $this->pagination->initialize($config);

      //$this->ag_auth->view('crud/manage', $tablename); // Load the view
      $data['pagetitle'] = array('Panel','Mis ordenes');
      $data['orders'] = $orders_uncompleted;
      $this->ag_auth->view('orders/my_orders', $data); // Load the view
   }
   }
   
   
   /**
    * fetches all the selled orders by the user
    *
    * @return void
    * @author adanowsky
    **/
   public function my_sells()
   {
      //$data['products'] = Product::where()
      //$data['orders'] = Order::all();//(array('conditions' => array('product_id in (?)', $products_id)));
      //$this->ag_auth->view('orders/my_sells', $data); // Load the view
      
      $this->load->library('pagination');
       $this->load->library('table');
       $this->table->set_heading('#orden', 'Producto', 'cantidad', 'total', 'fecha');
       $shop = _has_shop(username());
       //needs an if $shop in order to figure it out if is seller and show him the info...
       if(!$shop){
          redirect("shops/create_new");
       }
       
       else{
       $shop_id = $shop->id;
       $products_id = array();
       $products = Product::all(
         array('select' => 'id',
               'conditions'=>array('shop_id = ?', $shop_id)));
       foreach($products as $product){
          $products_id[] = $product->id;
       }

       if(! empty($products_id))
       {
       $orders = Order::all(array('conditions' => array('product_id in (?)', $products_id)));
       $tablename = "Sells";
       $orders_uncompleted = Order::find('all', 
                                      array(
                                         'conditions' => array(
                                                           'product_id in (?) AND completed IS NULL ', $products_id),
                                                           'limit' => 10,
                                                           'offset' => $this->uri->segment(4)
                                         ));
        }
        else{

          redirect('admin');
        }

        //needs eaggy loading or sth like that in order to retrive only once the product name from product_id
       foreach ($orders_uncompleted as $uncompleted){
          if($uncompleted->completed != "NULL"){
             $actions = anchor("admin/orders/rate/".$uncompleted->id."/", "Calificar"); // Build actions links
          }
          else{
             $actions = NULL;
          }
          $this->table->add_row(
             $uncompleted->id, 
             $uncompleted->product_id,
             $uncompleted->quantity,
             $uncompleted->total,
             $uncompleted->date,
             $actions); // Adding row to table
       }
        $config['base_url'] = site_url().'/admin/orders/my_sells';      
          $config['per_page'] = 10;
          $config['total_rows'] = count($orders);      
          $this->pagination->initialize($config);
          
          //$this->ag_auth->view('crud/manage', $tablename); // Load the view
          $data['orders'] = $orders_uncompleted;
          $data['pagetitle'] = array('Panel','Mis ventas');
          $this->ag_auth->view('orders/my_sells', $data); // Load the view
          
       }
       
   }
   
   
   public function create_new()
   {
      if(logged_in() === TRUE){
         $user = User::find_by_username(username());
         $profile = _has_profile($user);
         if($profile){         
            $product = Product::find($this->input->post('product_id'));      
            $attributes['quantity'] = $this->input->post('quantity');
            $attributes['profile_id'] = $user->profile->id;
            $attributes['product_id'] = $product->id;
            $attributes['date']  = date("Ymd H:i:s");
            $attributes['total'] = $attributes['quantity'] * $product->price;
            $shop = _has_shop(username());
            $seller= $product->shop->profile;
            if(check_credit($seller->user->username) >= $attributes['total'] *.03){
               
               if(! is_null($shop)){
                  if($shop->id != $product->shop_id){            
                     $order = new Order($attributes); 
                     $order->save();
                     $this->_send_emails($attributes);
                     $product->quantity -= $order->quantity;
                     $product->save(false);
                    
                     Order::decrease_credit($seller, $order->total);                  
                     echo '{"status":"1",
                            "message":"successfully bought",
                            "url":"'.site_url().'/admin/orders/my_orders"}';
                  } else {
                     echo '{"status":"0",
                            "message":"cant buy your own product...", 
                            "url":"'.site_url().'/"}';
                  }
               } else {
                  $order = new Order($attributes); 
                  $order->save();
                  $this->_send_emails($attributes);
                  $product->quantity -= $order->quantity;
                  $product->save(false);
                  Order::decrease_credit($seller, $order->total);
                  echo '{"status":"1",
                         "message":"successfully bought",
                         "url":"'.site_url().'/admin/orders/my_orders"}';              
               }
            
            }                        
            else{
               echo '{"status":"0",
                 "message":"seller does not have enough credit..",               
                 "url":"'.site_url().'/profiles/create_new"}';               
            }
         }
         else{
          $product = Product::find($this->input->post('product_id'));
          $this->session->set_userdata("redirect", base_url("producto/".$product->name_url));
            echo '{"status":"0",
                   "message":"does not have profile, needs to complete her profile.",
                   "url":"'.site_url().'/profiles/create_new"}';
         }
      }
      else{
        $product = Product::find($this->input->post('product_id'));
        $this->session->set_userdata("redirect", base_url("producto/".$product->name_url));
         echo '{"status":"0",
                "message":"You are not logged in",
                "url":"'.site_url().'login"}';
      }
   }
   
   /**
    * Send the email with the relevant information for both parts
    *
    * @return void
    * @author adanowsky
    */
   private function _send_emails($attributes){
      $product = Product::find($attributes['product_id']);
      $data['seller'] = $product->shop;
      $data['buyer']  = Profile::find($attributes['profile_id']);
      $data['product'] = $product->name;
      $data['quantity'] = $attributes['quantity'];
      $data['total'] = $attributes['total'];
      $this->load->library('email');
      $this->_send_to_buyer($data);
      $this->_send_to_seller($data);

   }
   
   /**
    * send email to buyer parameters: ($product, $name, $telephone, $email) from seller
    *
    * @return void
    * @author adanowsky
    **/
    
   private function _send_to_buyer($data)
   {
      $to = $data['buyer']->user->email;
      
      $this->email->send_seller_info($to, $data);
      return;
   }
   
   private function _send_to_seller($data)
   {
      $to = $data['seller']->profile->user->email;
      
      $this->email->send_order($to, $data);
      return;
   }
   
}