<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rates extends Application {
   
   function __construct(){
      parent::__construct();
      $this->ag_auth->restrict();

   }
  
  public function show(){
     $user = User::find_by_username(username());
     $profile = Profile::find_by_user_id($user->id);
     try{
        $order = Order::find($this->uri->segment(4));
        $seller = $order->product->shop->profile;
        $buyer = $order->profile;

        if($buyer->id == $profile->id) {
           if($order->buyer_rate == 1){
              redirect("admin");
           }
           else{
              $data['i_am'] = "comprador";
              $data['order'] = $order;
              $this->ag_auth->view('rates/show', $data);
           }        
        }
        else if($seller->id == $profile->id){
           if($order->seller_rate == 1){
              redirect("admin");
           }
           else{
              $data['i_am'] = "vendedor";
              $data['order'] = $order;
              $this->ag_auth->view('rates/show', $data);
           }
        }
        else{
           redirect('admin');
        }
     }
     catch (ActiveRecord\RecordNotFound $e){
        redirect('404');
     }
  }
  
  
 public function rate()
 {
    
    //validate if the order corresponds to the user
    //check if the order is already rated by the user pending...
    //if is not rated yet: 
       //show rate form
    //else redirect
    $attributes = $this->input->post();
    if($attributes['completed'] == "1"){
       $attributes['reason'] = "0";
    }
    unset($attributes['submit']);
    $user = User::find_by_username(username());
    $profile = Profile::find_by_user_id($user->id);
    $order_id = $attributes['orders_id'];
    $order = Order::find($order_id);
    $seller = $order->product->shop->profile;
    $buyer = $order->profile;
    $rate = new Rate($attributes);
    //if is buyer rates the seller of the product in the order:
    if($order->profile_id == $profile->id){
       if($order->buyer_rate == 1){
          redirect('admin');  
       }
       else{
          $rate->profile_id = $profile->id;
            $rate->points($seller, $rate->value, "seller");
            if($rate->completed == 1){
               $seller->total_sales += 1;
               $seller->save();
               $order->buyer_rate = 1;
            }
            else{
               $order->buyer_rate = 1;
            }
       }
    }
    //if is seller rates the buyer of the order..:
    else if($seller->id == $profile->id){
       if($order->seller_rate == 1){
          redirect("admin");
       }
       else{
          $rate->profile_id = $profile->id;
          $rate->points($buyer, $rate->value, "buyer");
          if($rate->completed == 1){
              $buyer->total_boughts += 1;
              $buyer->save();
              $order->seller_rate = 1;
           }
           else{
              $order->seller_rate = 1;
           }
       }
    }
    
    else{
       redirect('admin');
    }
    $rate->save();
    if($order->seller_rate == 1 && $order->buyer_rate == 1){
       $order->completed = 1;
    }
    $order->save();
   
    redirect("admin");
 }
}
?>