<?php
class Profiles extends Application {
		//refactor is needed in this controller for security issues!
		public function __construct() {
            parent::__construct();
       }
       
       public function authorization() {
       		if ( !logged_in() ) {
       			redirect("login");
       		}
       }
       
       public function show(){
         $uri=$this->uri->segment(3);
    	   try{
    	      $data['profile'] = Profile::find($uri);
            $data['pagetitle'] = 'Perfil de '.$data['profile']->user->username;
            $yield = $this->load->view('profiles/show', $data, true);
            $this->load->view("layouts/application", array('yield' => $yield));
          }
          catch (ActiveRecord\RecordNotFound $e){
               redirect('404');
          }
       }
       
       
       public function create_new(){
          $data['user'] = User::find_by_username(username());
          if ( _has_profile( $data['user'] ) ){
             redirect('admin/dashboard');
          }
          else{
          	 $data['pagetitle'] = array('Panel', 'Crear perfil');
             $data['tablename'] = "Profile";
             $this->ag_auth->view('profiles/new.php', $data);
          }
       }
       
       public function edit(){
          $data['user'] = User::find_by_username(username());
          if($this->_has_profile( $data['user'] ) ){
               redirect('profiles/create_new');
          } else {
          	 $data['pagetitle'] = array('Panel', 'Editar perfil');
             $data['profile'] = $data['user']->profile;
             $this->ag_auth->view('profiles/edit.php', $data);
         }
       }
//save???
       public function edit_save(){
        $data['user'] = User::find_by_username(username());
          if($this->_has_profile( $data['user'] ) ){
               redirect('admin/dashboard');
          }
          else{
            $attributes = $this->input->post();
            $data['user'] = User::find_by_username(username());
            $profile = Profile::find_by_user_id($data['user']->id);
            unset($attributes['submit']);
            unset($attributes['user_id']);
            $profile->update_attributes($attributes);
            redirect('admin/dashboard');
          } 
       }
       
       public function save_new(){
          $data['user'] = User::find_by_username(username());
          if ( _has_profile( $data['user']) ){
             redirect('admin/dashboard');
          }
          else{          
          $attributes = $this->input->post();
          //var_dump($attributes);
          //die;
          if($attributes){
            unset($attributes['submit']);
            $profile = new Profile($attributes);
            $saved = $profile->save();
            if($profile->is_valid()){
              if($saved == true){
               redirect($this->session->userdata("redirect"));
              }
              else{
              	$data['pagetitle'] = array('Panel', 'Crear perfil');
               $data['user'] = User::find_by_username(username());
               $data['tablename']        = "Profile";
               $data['errors'] = $profile->errors->full_messages();
               $this->ag_auth->view('profiles/new.php', $data);             
              }
            } else {

              $this->session->set_flashdata("first_name", $_POST["first_name"]);
              $this->session->set_flashdata("last_name", $this->input->post("last_name"));
              $this->session->set_flashdata("telephone", $this->input->post("telephone"));
              $this->session->set_flashdata("postal_code", $this->input->post("postal_code"));
              $this->session->set_flashdata("address", $this->input->post("address"));
              
              $this->session->set_flashdata("errors", $profile->errors->full_messages());              
              redirect("profiles/create_new");        
            }
          }
          else{
            redirect('profiles/create_new');
          }
        }
       }
       //meter esta funcion al helper
       
       private function _has_profile($user){
          $profile = $user->profile;
          return is_null($profile);
       }
       
}


?>