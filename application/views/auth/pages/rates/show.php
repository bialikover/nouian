	<h1> Calificar</h1>
	<div class="productpanel">
		<div id="productpanelimage">
    	<?php if ( ! is_null($order->product->images[0]->image_url)):?>
         <?php echo anchor("producto/".$order->product->name, 
            '<img id="productpanelimg" width = "100" height = "100" src = "'.base_url() ."images/product/100x100". $order->product->images[0]->image_url.'"/>'
         );?>
        <?php else:?>
            <?php echo anchor("products/show/".$order->product->id, 
               '<img id="productpanelimg" width = "100" height = "100" src = "'.base_url() .'/assets/images/noimage'.rand(0,4).'.png"/>');?>
        <?php endif;?>
		</div>
		<div id="productpaneltext">
			<div id="productpaneltitle">
				<span><?php echo anchor("products/show/".$order->product->id , $order->product->name)?></span>
				<?php if($i_am =="comprador"):?>
				<div id="vendorpanel">
				<?php echo anchor("profiles/show/".$order->product->shop->profile->id, 
				                                  "Vendedor: ". $order->product->shop->profile->user->username );?>
				</div>
				<?php else:?>
				   <div id="vendorpanel">
   				<?php echo anchor("profiles/show/".$order->product->shop->profile->id, 
   				                                  "Comprador: ". $order->profile->user->username);?>
   				</div>
				<?php endif;?>
				   
			</div>
			<div id="productpanelprice">
				<span>$<?php echo $order->product->price;?></span><span> X <?php echo $order->quantity;?></span>
			</div>
			<div id="productpanelpieces">
				<span><?php echo $order->total;?></span>
			</div>
			<div id="productpanelaccions">
			<span><?php echo RelativeTime($order->date);?></span>
			</div>
		</div>
	</div>	
	<div class="clear"></div>
	<div id="rate">
      <?php echo form_open('admin/rate/order');?>
      <?php echo form_hidden('orders_id', $order->id); ?>
			<h4>¿Se concreto el intercambio?</h4>	
			<table>
			<tbody>
			<tr>
			<td>Si</td>
			<td><?php echo form_radio('completed', '1', TRUE); ?></td>
			</tr>
			<td>No</td>
			<td><?php echo form_radio('completed', '0',  FALSE); ?></td>
			<tr>
			</tbody>
			</table>
			<div id="rateno">
				   <div id = "reason" style ="display:none">
      				<h4>¿Porque?<h4>
                  <p>Describe el motivo por el cual no se completo la transaccion</p>
               <?php   $options = array( "Me arrepentÌ de comprarlo",
                                         "No tenÌa el dinero en ese momento",
                                         "El vendedor me contactó, pero no pude responderle",
                                         "El vendedor no respondió a mis mensajes", 
                                         "El vendedor no intentó contactarme", 
                                         "El vendedor se arrepintió de venderlo", 
                                         "El vendedor se quedó sin inventario", 
                                         "La descripción del artÌculo no se correspondía con el mismo ",
                                         "El vendedor no respetó las políticas de envÌo y pago del artículo ",
                                         "Otros motivos por responsabilidad del vendedor"
                                  );



                  echo form_dropdown('reason', $options);
                  ?>

               </div>
            
			</div>
			<h4>¿Como calificas a tu contraparte?</h4>						
			<table>
			<tbody>
			<tr id = "positive">
			<td>Positivo</td>
			<td><?php echo form_radio('value', 'positive', FALSE);?></td>
			</tr> 
			<tr>
			<td>Neutral</td> 
			<td><?php echo form_radio('value', 'neutral', TRUE);?></td>
			</tr> 
			<tr>
			<td>Negativo </td>
			<td><?php echo form_radio('value', 'negative', FALSE);?></td>
			</tr>
			</tbody>
			</table>
			<?php if($i_am == "comprador"):?>
			   <h4>Deja tu comentario respecto a este vendedor </h4>
			<?php else:?>						
		      <h4>Deja tu comentario respecto a este comprador </h4>
		   <?php endif;?>
			<?php echo form_textarea("comment", "", "max-length='100'");?>
			<div class="clear"></div>
			<?php echo form_submit("submit", "Calificar", "class='botonpanel'")?>
         <?php echo form_close();?>
	</div>

