		<div id="nav">
				<a><span>
				<?php echo anchor("/","Home");?> > 
				<span id="navrojo"> 
				   <?php echo anchor("categoria/".$product->category->name_url ,$product->category->name); ?>
				</span></span></a>
				</div><!--Termina nav=-->
				<div id="product">
					<div id="left">
						<div id="thumbs">
                  <?php if($product->images!= null):?>
							<div id="main-imgdiv">
								<ul>
								<?php foreach ($product->images as $image):?>
									<li>
										<div class="mainimgcont">
										<a id="single_image" rel="group1" href="<?php echo base_url()."images/product/full".$image->image_url;?>">
											<div class="mainimg">
											<img alt = "<?php echo $product->name;?>" src="<?php echo base_url()."images/product/370x370". $image->image_url;?>" />
											</div>
										</a>
										</div>
									</li>
								<?php endforeach;?>	
								</ul>
							</div>
						<?php else:?>
							<div id="main-imgdiv">
								<img alt="nombreproducto" id="main-img" class="item-3 drop-shadow" src="<?php echo base_url().'/assets/images/noimage'.rand(0,4).'.png' ?>"></img>
							</div>
						<?php endif;?>
						<div id="thumbslist">
							<ul>
								
							  <?php foreach ($product->images as $image):?>
							<li>
							  <img class="thumbimg" alt = "<?php echo $product->name;?>" src="<?php echo base_url()."images/product/100x100". $image->image_url;?>" />
							</li>
								<?php endforeach;?>
							</ul>
						</div>
						</div>
					</div> <!--end product left=-->	
					<div id="right">
						<div id="product-info">
							<article>
						   <div style="position: relative; float: left; margin-right: 12px; margin-top:6px; width: 70px; height: 65px;">
							  <div class="fb-like" data-send="false" data-layout="box_count" data-width="450" data-show-faces="true"></div>
							</div>
							<h1><?php echo $product->name;?></h1>
							<input type="hidden" id = "product_id" value="<?php echo $product->id;?>" />
							<input type="hidden" id="path_order_create_new" value="<?php echo site_url("admin/orders/create_new");?>">
							<h2>por <?php echo anchor($product->shop->name_url,$product->shop->name);?> </h2>
							<div id="tienda-acceso"><?php echo anchor($product->shop->name_url ,"< Ir a esta tienda");?>
							  </div>
							<div id="price-area">
							<strong class="price">$<?php echo $product->price;?> </strong>
							<span id="cuantos">
							<span>¿Cuantos?</span>
							
							<?php if($product->quantity > 0):?>
							<select name="installmentSelect" class="def_select" id="installmentSelect">
							   <?php for ($i = 1; $i  <= $product->quantity; $i++ ):?>
							      <option value="<?php echo $i ?>"><?php echo $i ?></option>
							   <?php endfor;?>
							</select>
							</span>
							<!--<span id="boton"><div class="boton"> Comprar</div></span>-->							
  				    		<?php echo '<span id="boton"><div class="boton"> Comprar</div></span>';?>
							<?php if(logged_in()=== TRUE && is_owner($product->id, username())):?>
      				    <?php echo anchor("products/edit/".$product->id,"editar producto" );?>
      			    <?php endif;?>
							</div>
							<p><?php echo $product->description;?></p>
						   <?php else:?>
						   <span> Woops parece que ya no hay!</span>
						   <?php endif;?>
							
						</div><!--end product info-->
						<div id="details">
							<h3>Mas informacion</h3>	
							<p>Pequeña descripcion tecnica del producto de menos de diez palabras</p>
							<ul class="detalles">

							<li class="noBorderTop">
							<span class="info-li">
							<label>Opciones de pago</label>
							<span><?php echo $product->payment_opt;?></span>
							</span>
							</li>
							<li class="noBorderTop">
							<span class="info-li">
							<label>Opciones de envio</label>
							<span><?php echo $product->shipping_opt;?></span>
							</span>
							</li>
							<li class="noBorderTop">
							<span class="info-li">
							<label>Material</label>
							<span><?php echo $product->material;?></span>
							</span>
							</li>
							<li class="noBorderTop">
							<span class="info-li">
							<label>Color</label>
							<span><?php echo $product->color;?></span>
							</span>
							</li>
							<li class="noBorderTop">
						   <span class="info-li">
							<label>Lugar</label>
							<span><?php echo $product->shop->profile->state;?></span>
							</span>
							</li>
							
							</ul>
						</div><!--end product details=-->
							</article> <!--end article=-->
					</div><!--end product right=-->
				</div><!--end product=-->
				<div class="clear"></div>
				<div id="quesans">
				<?php if(logged_in()=== TRUE && !is_owner($product->id, username())):?>
				<h3>¿Tienes alguna duda respecto a este articulo? </h3>
				<div id="formquest">
				   <?php echo form_open('questions/save');?>
				   <input type="hidden" id="path_question_save" value="<?php echo site_url("questions/save");?>">
				   <?php echo form_hidden("profile_id", username());?>
				   <?php echo form_hidden("product_id", $product->id);?>
				   <?php echo form_textarea(array("name"=>"content", "id"=>"questimput"));?>
				   <div class="clear"></div>
					<span id="tip">No ingreses datos de contacto, no uses lenguaje vulgar ni ofertes o preguntes por otro artículo</span>
				   <?php echo form_submit(array("name"=>"submit", "class"=>"boton"), "pregunta!");?>
				   <?php echo form_close();?>
				</div>
				<?php elseif(logged_in()=== TRUE && is_owner($product->id, username())): ?>
				
				<?php else:  ?>
				<h3>¿Tienes alguna duda respecto a este articulo? </h3>
					<div id="formquest">
					<form action="<?php echo base_url().'login/' ?>">
				   <textarea id="questimput"> </textarea>
				   <div class="clear"></div>
					<span id="tip">No ingreses datos de contacto, no uses lenguaje vulgar ni ofertes o preguntes por otro artículo</span>
				   <input type="submit" class="boton"/>
				</div>
				<?php  endif;?>
				<div id="questspace">
				   <dl>
					<?php foreach(array_reverse($product->questions) as $question):?>
   					<?php if( ! is_null($question->answer)):?>
<!--      					<p><?php echo anchor( "profiles/show/".$question->profile->id, $question->profile->user->username) ?> pregunto:</p>-->
      					<dt><?php echo $question->content;?></dt>
<!--      					<p>Respuesta del vendedor:</p>-->
      					<dd><span>Respuesta del vendedor: </span><?php echo $question->answer;?> 
      					   <span class="timeanswer"><?php echo RelativeTime($question->answered_at);?></span>
      					</dd>
      				<?php endif;?>
   				<?php endforeach;?>
					</dl>
					<dl>
           </div>
</div>
		