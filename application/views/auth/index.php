
<?php

$this->load->view($this->config->item('auth_views_root') . 'header');
?>
<?php $this->load->view($this->config->item('auth_views_root') . 'sidebar'); ?>
<div id="panel-resume">
	<div id="panel-resume-content">	
<?php
if(isset($data))
{
	$this->load->view($this->config->item('auth_views_root') . 'pages/'.$page, $data);
}
else
{
	$this->load->view($this->config->item('auth_views_root') . 'pages/'.$page);
}
?>
</div><!-- termina panel-resume-content -->
</div> <!-- termina	panel-resume-->
<?php 
$this->load->view($this->config->item('auth_views_root') . 'footer');

?>