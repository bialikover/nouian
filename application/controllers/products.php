<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends Application {
   

   function __construct(){
      parent::__construct();
   }
   
   function my_products(){
   	$pagetitle = 'Mis productos';
  		$shop = _has_shop(username());
  		$data['products'] = Product::find(
  			'all', 
  			array("shop_id" => $shop->id)
  		);
  		$this->ag_auth->view("products/my_products", $data);
   }
   
    /**
     * search products
     *
     * @return array of products
     * @author adanowsky bialikover
     **/
    public function search() {
    	if( $this->input->post("word") != null ){
    		$criteria  = $this->input->post("word");
    		
         $data['pagetitle'] = 'Buscar "'.htmlentities($criteria).'"';
         $data['products'] = Product::find_by_sql("select * from products where name like '%$criteria%' or description like '%$criteria%';");
         /*$data['products'] = Product::find(
          	'all', 
          	array(
          		'conditions' => array(
          			'name LIKE "%?%" OR description LIKE "%?%"',
          			array(
          				$criteria, $criteria
          			)
          		)
          	)
         );*/
         
         $data['categories'] = Category::find('all', array('order' => 'name ASC'));
         $data['criteria'] = $criteria;
         $data['featured'] = Product::find(
         	'all',
         	array(
         		'conditions' => array(
         			"featured" => 1
         		),
         		"limit" => 4,
         		"order" => "RAND()"
         	)
         );
         //var_dump($data['criteria']);
         //var_dump($data['products']);
         //die();
         $yield = $this->load->view('products/search', $data, true);
         $this->load->view("layouts/application", array('yield' => $yield));
        } else {
          redirect("products/index");
        }
    }
    
    
    /**
     * 
     *
     * @return void
     * @author adanowsky bialikover
     **/
    public function edit() {
        $product_id = $this->uri->segment(3);
		$data['product'] = Product::find($product_id);
		
        if ( is_owner($product_id, username()) ) {
            $data['categories'] = Category::find('all', array('order' => 'name ASC'));
            $data['pagetitle'] = 'Editar "'.$data['product']->name.'"';
            
            $this->ag_auth->view("products/edit", $data);
        } else {
            redirect("producto/" . $product->name_url);
        }
    }
    
    public function edit_save(){
        $attributes = $this->input->post();
        $product_id = $this->input->post('product_id');
        $product    = Product::find($product_id);
        
        if( is_owner($product_id, username())){
            unset($attributes['submit']);
            unset($attributes['product_id']);
            
            $product->quantity    = $this->input->post('quantity');
            $product->description = $this->input->post('description');
            $product->material    = $this->input->post('material');
            $product->color       = $this->input->post('color');
            $product->finished    = $this->input->post('finished');
                                        
            $product->save();
            $this->upload_photo();
        } else{ //not the owner
            redirect("producto/" . $product->name_url);
        }
    }
    
	public function show() {
		$uri=$this->uri->segment(2);	   
		$data['product'] = Product::find_by_name_url($uri);
		
		if ( !is_null($data['product']) ) {
			$data['pagetitle'] = array($data['product']->shop->name, $data['product']->name);
			$yield = $this->load->view('products/show', $data, true);
			$this->load->view("layouts/application", array('yield' => $yield));
		} else {
         redirect('404');
      }
	}
	
	//product listin'
	public function index() {
		$data['pagetitle'] = 'Productos';
		$data['products'] = Product::all();
      $yield = $this->load->view('products/index', $data, true);
      $this->load->view("layouts/application", array('yield' => $yield));
	}
	
	public function create_new(){
	   $data['categories'] = Category::find('all', array('order' => 'name ASC'));
	   $data['shop'] = _has_shop(username());
	   $data['pagetitle'] = array('Panel','Nuevo producto');
	   $credit = check_credit(username());
	   
	   if( $credit != null ) { //does not have credit so i need to redirect to my shop create a new shop!
			if ( $credit < 3 ) {
				//does not have enough credit...
				redirect("/admin");
      	}
      	
      	$this->ag_auth->view('products/new.php', $data);
      } else {
   		redirect("shops/my_shop");
      }
	}
	
	public function save_new(){
     $data['shop'] = _has_shop(username());
     $credit = check_credit(username());
     
     if($credit != null){ //does not have credit so i need to redirect to my shop create a new shop!
       if( $credit < 3 ){   
          redirect("/admin");
        } else {
          if( $this->input->post() != false){
            $attributes['product'] = $this->input->post();

             unset($attributes['product']['payment1']);
             unset($attributes['product']['payment2']);
             unset($attributes['product']['delivery1']);
             unset($attributes['product']['delivery2']);
             unset($attributes['product']['submit']);                         
              $attributes['attributes'] = Product::set_attributes();
              $attributes['shop'] = _has_shop(username());
              $product = new Product($attributes['product']);
              $product->payment_opt = $attributes['attributes']['payment_opt'];
              $product->shipping_opt = $attributes['attributes']['shipping_opt'];
              $product->shop_id = $attributes['shop']->id;
             //$product->name_url = Product::url_product($product->name);
             //$product->name_url = Product::validate_name_url($product->name_url);
             $product->url_product();

             if( $product->is_valid()){
              $saved = $product->save();
              if($saved == true){
                 $data['product_id'] = $product->id;
                 $this->ag_auth->view("products/upload_photos.php", $data);
                
              }
              else{
             /*     $this->session->set_flashdata("name", $_POST["name"]);
              $this->session->set_flashdata("quantity", $this->input->post("quantity"));
              $this->session->set_flashdata("price", $this->input->post("price"));
              $this->session->set_flashdata("description", $this->input->post("description"));
              $this->session->set_flashdata("material", $this->input->post("material"));
              $this->session->set_flashdata("color", $this->input->post("color"));
              $this->session->set_flashdata("finished", $this->input->post("finished"));*/
                 echo "no se salvo";
                 $data['categories'] = Category::find('all', array('order' => 'name ASC'));
                 $data['shop'] = _has_shop(username());
                 $data['errors'] = $product->errors->full_messages();
                 //$this->ag_auth->view('products/new.php', $data);
                 redirect("products/create_new"); 
                 
              }
            }
            else{
              echo "no es valido";
              $this->session->set_flashdata("name", $_POST["name"]);
              $this->session->set_flashdata("quantity", $this->input->post("quantity"));
              $this->session->set_flashdata("price", $this->input->post("price"));
              $this->session->set_flashdata("description", $this->input->post("description"));
              $this->session->set_flashdata("material", $this->input->post("material"));
              $this->session->set_flashdata("color", $this->input->post("color"));
              $this->session->set_flashdata("finished", $this->input->post("finished"));
              $this->session->set_flashdata("errors", $product->errors->full_messages());              
              redirect("products/create_new");               
            }
          }
          else{
            redirect("products/create_new");    
          } 
        }   
      }
      else{
        redirect("shops/my_shop");
      }

	}
	
	
	public function upload_photo(){	
	   $product_id = $this->input->post('product_id');
      //	echo var_dump($_FILES);
	   $shop = _has_shop(username());
      $path = "./assets/images/shops/$shop->id/$product_id";
      $path2 = "/$shop->id/$product_id";
	   $total_images = 0;
	   if(!file_exists($path)){
	 //  echo "no existe deberia de crearlo"; 		
          $file_path = $this->_create_file_path($product_id);
	   }
      $images = $this->_do_upload($product_id, $path);
      foreach($images as $image){
	      if($image['is_image']){
	         # echo var_dump($image_attributes);
	         $total_images += 1;
	         #die;
	         $image_attributes['image_url'] = $path2."/".$image['file_name'];
	         $image_attributes['product_id'] = $product_id;
      	  # echo var_dump($image_attributes);
      	  # die;
            $image = new Image($image_attributes);
            $image->save();
	      }
	   }
   	if($total_images == 0){
   	   $data['product_id'] = $product_id;
   	   $data['error'] = "por lo menos debes agregar una imagen";
   	   $this->ag_auth->view("products/upload_photos.php", $data);
   	}	
      
      $product = Product::find_by_id($product_id);
      
      redirect('producto/'.$product->name_url); 
	
	} 

	
	
	private function _do_upload($product_id, $path){
         //$shop = _has_shop(username());
         //$file_path = $this->_create_file_path($product_id);
         $config['upload_path'] = $path;//"./assets/images/shops/$shop->id/$product_id";
         $config['file_name'] = 'product';
         $config['allowed_types'] = 'gif|jpg|png|jpeg';
         $config['max_size']	= 1024 * 3;
         $this->load->library('upload');
         $this->upload->initialize($config);
	  //deberia de hacer validacion aqui
         //if ( ! $this->upload->do_upload())
         //{      
            $this->upload->do_upload('image1');
            //validacion...
         //	$error = array('error' => $this->upload->display_errors());                        
	  //	$this->load->view('shops/edit.php', $error);                 
      //   }                                                            
        // else                                                         
      //   {  
          $info[] = $this->upload->data();        
          $this->upload->initialize($config);
          $this->upload->do_upload('image2');
          $info[] = $this->upload->data();
          $this->upload->initialize($config);
          $this->upload->do_upload('image3');
          $info[] = $this->upload->data();
          $this->upload->initialize($config);
          $this->upload->do_upload('image4');
          $info[] = $this->upload->data();    
//echo var_dump($info);
//die;
  //          return $config['upload_path']."/".$info['file_name'];
      //   }                                                                  
    	   return $info;  
	}
	private function _create_file_path($product_id){
        $shop = _has_shop(username());
        $path = "./assets/images/shops/$shop->id/$product_id";
        mkdir($path, 0755);
        return $path;
     }
	
}
