

<h1>Mis compras:</h1>


<?php foreach ($orders as $uncompleted):?>
<div class="productpanel">
	<div id="productpanelimage">
	<?php if ( !is_null($uncompleted->product->images[0]->image_url)):?>
     <?php echo anchor("producto/".$uncompleted->product->name_url, 
        '<img id="productpanelimg" width = "100" height = "100" src = "'.base_url() ."images/product/100x100". $uncompleted->product->images[0]->image_url.'"/>'
     );?>
    <?php else:?>
        <?php echo anchor("producto/".$uncompleted->product->name_url, 
           '<img id="productpanelimg" width = "100" height = "100" src = "'.base_url() .'/assets/images/noimage'.rand(0,4).'.png"/>');?>
    <?php endif;?>
	</div>
	<div id="productpaneltext">
		<div id="productpaneltitle">
			<span><?php echo anchor("producto/".$uncompleted->product->name_url , $uncompleted->product->name);?></span>
		</div>
		<div id="productpanelprice">
			<span>$<?php echo $uncompleted->product->price?>.00</span><span> X <?php echo $uncompleted->quantity?></span>
		</div>
		<div id="productpanelpieces">
			<span>$<?php echo $uncompleted->total?>.00</span>
		</div>
		<div id="productpanelaccions">
		<ul id="productpanelmenu">
			<li>Acciones
				<ul>
				   <?php if($uncompleted->completed != "NULL"):?>
				      <li><?php echo anchor("admin/orders/rate/".$uncompleted->id."/", "Calificar");?></li>
               <?php endif;?>
					
					<li><a href="#">Datos Vendedor</a></li>
				</ul>
			</li>
		</ul>
		</div>
	</div>
</div>				
<!--<div class="productpanelend">
	<div id="productpanelimage">
		<a href="#"><img id="productpanelimg" src="images/testspon.jpg"></a>
	</div>
	<div id="productpaneltext">
		<div id="productpaneltitle">
			<span><a href="">Nombre del producto</a></span>
		</div>
		<div id="productpanelprice">
			<span>$800.00</span><span> X 1</span>
		</div>
		<div id="productpanelpieces">
			<span>$800.00</span>
		</div>
		<div id="productpanelaccions">
		<ul id="productpanelmenu">
			<li><a href="#">Acciones</a>
				<ul>
					<li><a href="#">Republicar</a></li>
					<li><a href="#">Editar</a></li>
				</ul>
			</li>
		</ul>
		</div>
	</div>
</div>-->	
<?php endforeach;?>