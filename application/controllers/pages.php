<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends Application {
   

	public function credit()
	{	
	  $yield = $this->load->view('pages/credit', "", true);
      $this->load->view("layouts/application", array('yield' => $yield));
	}
	
	public function faq(){
	   $yield = $this->load->view('pages/faq', "", true);
        $this->load->view("layouts/application", array('yield' => $yield));
	}
	
	public function ayuda(){		
		$yield = $this->load->view('pages/ayuda', "", true);
        $this->load->view("layouts/application", array('yield' => $yield));

	}
	
	public function terminos(){
	   $yield = $this->load->view('pages/terminos', "", true);
       $this->load->view("layouts/application", array('yield' => $yield));
	}
	public function blog(){
	   $yield = $this->load->view('pages/blog', "", true);
       $this->load->view("layouts/application", array('yield' => $yield));
	}
	public function acerca(){
		$yield = $this->load->view('pages/acerca', "", true);
       $this->load->view("layouts/application", array('yield' => $yield));
	}

	public function contacto(){
		$yield = $this->load->view('pages/contacto', "", true);
       $this->load->view("layouts/application", array('yield' => $yield));
	}

	
}

/* End of file pages.php */
/* Location: ./application/controllers/pages.php */