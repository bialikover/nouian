<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronjobs extends CI_Controller {
   /**
    * Enviar emails de recordatorio a quienes han comprado o vendido
    * y no han calificado aún
    */
   public function mail_rate_reminder() {
      $this -> load -> library('email');      
      $seller_mailed_profiles = array();
      $buyer_mailed_profiles  = array();
      $product_mailed_profiles = array();
      
      $trigger_date_unix  = strtotime('-1 week');
      $trigger_date       = date('Y-m-d', $trigger_date_unix);
      $trigger_date      .= ' 23:59:59';
      
      $unrated_orders = Order::all(array(
         'conditions' => array(
            'completed IS NULL AND date <= ?',
            $trigger_date
         )
      ));
      
      if ( !is_null($unrated_orders) ) {
         $this -> load -> library('email');
         
         foreach ( $unrated_orders as $order ) {
            // > Evitar llenar de SPAM al vendedor y al comprador (y ahorrar queries)
            if (
               isset($product_mailed_profiles[$order -> product_id]) &&
               isset($buyer_mailed_profiles[$order -> profile_id])
            ) continue;
            
            // > Obtener perfil del comprador, del vendedor, la tienda y el producto
            /*   [TODO] Buscar optimización de queries, pues de momento todos éstos
             *          son necesarios para generar las vistas de los emails
             */
            $data['buyer']   = Profile::find_by_id($order -> profile_id);
            $data['product'] = Product::find_by_id($order -> product_id);
            
            $data['shop']    = Shop::find_by_id($data['product'] -> shop_id);
            $data['seller']  = Profile::find_by_id($data['shop'] -> profile_id);
            
            // > Enviar email de recordatorio al Comprador si no se le ha enviado alguno antes
            if ( !$order -> buyer_rate && !isset($buyer_mailed_profiles[$order -> profile_id]) ) {
               $this -> email -> send_rate_reminder_buyer(
                  $data['buyer'] -> user -> email,
                  $data
               );
               
               $buyer_mailed_profiles[$order -> profile_id] = true;
            }
           
            // > Enviar email de recordatorio al Vendedor si no se le ha enviado alguno antes
            if ( !$order -> seller_rate && !isset($product_mailed_profiles[$order -> product_id]) ) {
               $this -> email -> send_rate_reminder_seller(
                  $data['seller'] -> user -> email,
                  $data
               );
               
               $product_mailed_profiles[$order -> product_id] = true;
            }
         }
      }
   }
}
