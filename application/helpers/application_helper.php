<?php
/*function get_user($username)
{
   $user = User::find_by_username($username);
   return $user;
}*/
	/* Acepta un arrego de un número indefinido de elementos. Si no se pasan argumentos, sólo se muestra Nouian. */
	/* Ejemplo: pagetitle(array('Perfumes', 'Florecitas Estrella')) devuelve 'Nouian :: Perfumes :: Florecitas Estrella' */
	function pagetitle($breadcrumb = array()) {
		$pagetitle = 'Nouian';
		$separator = '::';
		
		// Si no es arreglo, forzar que sea arreglo
		if ( !is_array($breadcrumb) )
			$breadcrumb = array($breadcrumb);
		
		// Validar que haya elementos, sino fallback a sólo título
		if ( count($breadcrumb) < 1 )
			return $pagetitle;
		
		// Finalmente concatenar elementos y devolver título
		$pagetitle .= ' ' . $separator . ' ' . $breadcrumb[0];
		unset($breadcrumb[0]);
		
		foreach ( $breadcrumb as $i )
			$pagetitle .= ' ' . $separator . ' ' . $i;
		
		return $pagetitle;
	}
?>