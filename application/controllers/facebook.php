<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Name:  Simple Facebook Codeigniter Login
 *
 * Author: Terry Matula
 *         terrymatula@gmail.com
 *         @terrymatula
 * Created:  03.31.2011
 *
 * Description:  An easy way to use Facebook to login
 *
 * Requirements: PHP5 or above
 *
 */
class Facebook extends Application {

    public $appid;
    public $apisecret;

    public function __construct()
    {
        parent::__construct();
        switch (ENVIRONMENT)
        {
            case 'development':
                $this->appid = '311397385570136';
                $this->apisecret = 'af0798ea531efcf3c2e06126b71af6e0';
            break;
            case 'testing':
                $this->appid = '488259604553242';
                $this->apisecret = 'ff489f00ca0cc45ecb57e95bde1f27cf';
            break;
            case 'production':
            //crear una nueva...
                $this->appid = '389021894522056';
                $this->apisecret = 'c529e440efd60d46d5ad5a84f5df9abc';
            break;        
        }
        
        // replace these with Application ID and Application Secret.
        
    }

    /**
     * if you have a Facebook login button on your site, link it here
     */
    public function index()
    {
        // set the page you want Facebook to send the user back to
        $callback = site_url('facebook/confirm');
        // create the FB auth url to redirect the user to. 'scope' is
        // a comma sep list of the permissions you want. then direct them to it
        $url = "https://graph.facebook.com/oauth/authorize?client_id={$this->appid}&redirect_uri={$callback}&scope=email,publish_stream";
        redirect($url);
    }

    /**
     * Get tokens from FB then exchanges them for the User login tokens
     */
    public function confirm()
    {
        // get the code from the querystring
        $redirect = site_url('facebook/confirm');
        $code = $this->input->get('code');
        if ($code)
        {
            // now to get the auth token. '__getpage' is just a CURL method
            $gettoken = "https://graph.facebook.com/oauth/access_token?client_id={$this->appid}&redirect_uri={$redirect}&client_secret={$this->apisecret}&code={$code}";
            $return = $this->__getpage($gettoken);
            // if CURL didn't return a valid 200 http code, die
            if (!$return)
                die('Error getting token');
            // put the token into the $access_token variable
            parse_str($return);
            // now you can save the token to a database, and use it to access the user's graph
            // for example, this will return all their basic info.  check the FB Dev docs for more.
            $infourl = "https://graph.facebook.com/me?access_token=$access_token";
            $return = $this->__getpage($infourl);
            if (!$return)
                die('Error getting info');
            $info = json_decode($return);
            $this->create_or_login($info);
        }
    }

    /**
     * CURL method to interface with FB API
     * @param string $url
     * @return json 
     */
    private function __getpage($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // check if it returns 200, or else return false
        if ($http_code === 200)
        {
            curl_close($ch);
            return $return;
        }
        else
        {
            // store the error. I may want to return this instead of FALSE later
            $error = curl_error($ch);
            curl_close($ch);
            return FALSE;
        }
    }

    private function create_or_login($fbinfo)
    {

        $user = User::find_by_email_or_username($fbinfo->email, $fbinfo->username);


        //verifica si el usuario no existe en yotly
        if($user == NULL){
            //crea al usuario
            $attributes = array('username'=> $fbinfo->username, 
                                  'email'=>$fbinfo->email, 
                                  'identifier' =>$fbinfo->id);
            $user =  User::create($attributes);
            $profile_attr = array('user_id'=> $user->id, 
                'first_name'=> $fbinfo->first_name, 
                'last_name'=> $fbinfo->last_name);
            $profile = Profile::create($profile_attr);
            $user_data = $this->ag_auth->get_user($user->username, 'username');
            unset($user_data['password']);
            unset($user_data['id']);                
            $this->ag_auth->login_user($user_data);
            redirect($this->session->userdata("redirect"));
        }
        else{
            //si ya tiene cuenta en yotli pero no con fb...
            if($user->identifier == null){
                $user->identifier = $fbinfo->id;
                $user->save();
                $user_data = $this->ag_auth->get_user($user->username, 'username');
                unset($user_data['password']);
                unset($user_data['id']);                
                $this->ag_auth->login_user($user_data);
                redirect($this->session->userdata("redirect"));
            }
            //si ya tiene cuenta con fb..
            else{ 
                $user_data = $this->ag_auth->get_user($user->username,'username');              
                unset($user_data['password']);
                unset($user_data['id']);
                $this->ag_auth->login_user($user_data);
                redirect($this->session->userdata("redirect"));
            }
        }
        

    }

}