<?php

class Shop extends ActiveRecord\Model

{
   static $before_validation = array('url_shop');
   static $validates_presence_of = array(
         array('profile_id'), 
         array('name', 'message' => 'Debes ingresar el nombre de tu tienda' ),
         array('description', 'message' => 'Debes ingresar la descripción de tu tienda')

   );
    
   static $validates_size_of = array(
     array('name', 'minimum' => 5, 'too_short' => 'El nombre debe tener minimo 5 caracteres!'),
     array('name', 'maximum' => 38, 'too_long' => 'El nombre debe tener máximo 5 caracteres!'),
     array('description', 'minimum' => 50, 'too_short' => 'La descripcion de tu tienda debe tener minimo 50 caracteres!'),
     array('description', 'maximum' => 538, 'too_long' => 'La descripcion de tu tienda debe tener maximo 538 caracteres!')
   );
  static $validates_uniqueness_of = array(
      array('name_url')
      //array(, 'message' => 'Ese nombre ya existe')
    );
  
  static $belongs_to = array(
   	array('profile')
   );
   static $has_one = array(
      array('credit')
   );
   
   static $has_many = array(
      array('products')
      
   );


   public function url_shop(){

   $this->name_url = strtolower( preg_replace("![^a-z0-9]+!i", "-", $this->name));
    //htmlspecialchars(str_replace(" ", "", strtolower($this->name)),ENT_QUOTES);

   }



}