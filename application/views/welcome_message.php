<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Nouian - Compra y vende productos hechos a mano</title>
  <meta name="description" content="">
	<!--Inician Facebook OG Headers=-->
	<meta property="og:title" content="Nouian - Compra y vende productos hechos a mano" />
	<meta property="og:description" content="Bazar online de arte, artesania y diseño mexicano" />
	<meta property="og:url" content="<?php echo base_url();?>assets/images/logo2.png" />
	<meta property="og:locale" content="es_MX" />
	<meta property="og:site_name" content="Nouian" />
	<!--Terminan Facebook OG Headers=-->
  <meta name="viewport" content="width=device-width">
	<link rel="shorcut icon" href="assets/images/favicon.ico">
	<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/vnd.microsoft.icon" />
	<link rel="icon" href="assets/images/favicon.ico" type="image/vnd.microsoft.icon" /> 
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/nouian.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.masonry.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/nouian.js"></script>
	<script type="text/javascript">
	 jQuery(document).ready(function(){
	 
	$('#hideshowsearch').toggle( function(){
		$(this).css({'background-position':' -313px -119px'});
		$('#searchback').animate({
		top:'53px'
		}, 200)
	} , function(){
		$(this).css({'background-position': '-288px -120px'});
		$('#searchback').animate({
		top:($(window).height() - $('#searchback').outerHeight())/2
		}, 200)
	});
	$('#contenido').find('.imgbox').hover(
		function(){
		 $(this).find('#spacebckindex').stop(true,true).fadeIn('slow');
				},
		function(){
		$(this).find('#spacebckindex').stop(true,true).fadeOut('slow');
		}
	);
	});
  </script>
</head>
<body onload="masonry()">
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
  <header>
     	<div id="headerwrapper">
			<div id="header-index">
			<div class="headsection">
				<?php echo anchor(base_url(), " <div id='logo'><h1><span>Nouian</span></h1></div>")?>
			</div>
			<div class="headsection">
				<div id="menu">
				<div id="actions">
				<nav> 
					<ul>
						<li><?php echo anchor("products/create_new","Vende");?></li>
						<li><?php echo anchor("shops","Descubre");?></li>
					</ul>
				</nav>
				</div>
				</div>
			</div>
			<div class="headsection">
				<div id="user"> 
				<?php if(username()):?>
				  <span id="usernamehead"><?php echo anchor("admin/dashboard", username());?></span>
				   <span id="closession"><?php echo anchor('logout', 'Cerrar sesi&oacute;n'); ?></span>
				<?php else:?>
				   <span id="signinhead"><?php echo anchor("/login", "Conectate");?>
				<?php endif;?>
				   </div>
			</div>
		</div>
      </div><!--Termina header=-->
  </header>
  
<div role="main" id="indexpage">
<div id="content-index">
	<div class="clear"></div>
	<div id="cochinada"></div>
	<div id="content-index">
			 <div id="contenido">	
					<div id="searchback">
						<div id="searchprincipal">
							<div id="searchform"><?php echo form_open("products/search", "buscar");?>
							<input type="text" class="search-home" maxlength="30" name="word" id="query" tabindex="1"/>
							<input  src="<?php echo base_url();?>assets/images/search1.png" type="image" class="search-button-home" accesskey="b" tabindex="2"/>
							</form>
							</div>
							<div id="hideshowsearch"></div>						
						</div>	
					</div> <!--end searchback=-->
				<?php if($products):?>
				   <?php foreach(array_reverse($products) as $product):?>
					  <?php if($product->images != null && $product->quantity > 0):?>
						<div class="imgbox">
						 <?php echo anchor("producto/$product->name_url",'<img src ="'.base_url()."/images/product/200x200".$product->images[0]->image_url.'"/>');?>
							<div id="spacebckindex">
								<a href="producto/<?php echo $product->name_url; ?>">
									<div id="shop-text">
										<span id="shop-title"><?php echo $product->name;?> </span>
										<span id="shop-title">$<?php echo $product->price;?></span><br>
										<span id="short-descript"><?php echo substr($product->description, 0, 32)."...";?></span>  
									</div>
								</a>
							</div>	
						</div>			
					 <?php /*
						<!--<div class="imgbox">
						  <?php //echo anchor("producto/$product->name_url",'<img src ="'.base_url().'/assets/images/noimage'.rand(0,4).'.png'.'"/>');?>
						</div>*/
					  endif;?>
				   <?php endforeach;?>
				<?php else:?>
				   <h1>Aun no hay productos...</h1>
				<?php endif;?>
	</div>
</div>

	<div id="footerwrap">
	<div id="footer-index">
			<div id="content-footer-index">
			<div id="menufooter">
			<ul>
						<li><a href="<?php echo base_url();?>faq/ayuda">Ayuda</a></li>
						<li><a href="<?php echo base_url();?>faq/terminos">Terminos</a></li>
			</ul>
			</div><span>Nouian © 2012</span>
			</div>
			</div>
	</div>
	</div>
   <script>
   function masonry(){
	 $('#contenido').masonry({
	    itemSelector: '.imgbox',
	    columnWidth: 0,
	    isAnimated: true,
	    animationOptions: {
		duration: 350,
		easing: 'swing',
		queue: false
		}});
		}
     var _gaq=[['_setAccount','UA-30778602-1'],['_trackPageview']];
     (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
     g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
     s.parentNode.insertBefore(g,s)}(document,'script'));
   </script>

</body>

</html>