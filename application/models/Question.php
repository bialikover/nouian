<?php

class Question extends ActiveRecord\Model  
{
  
  static $validates_presence_of = array(
     array('answer')
   );
   
   static $belongs_to = array(
   	array('product'),
   	array('profile')
   );
}