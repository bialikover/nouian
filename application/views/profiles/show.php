<h1><?php echo $profile->user->username;?></h1>
<img src = "<?php echo get_gravatar($profile->user->email);?>"/>
<h2>Reputacion:</h2>
<?php echo $profile->overall_buyer;?>
<h3>Total de compras</h2>
<?php echo $profile->total_boughts;?>
<?php if($profile && $reputation):?>
 <div id = "personal-info">
    <h3>Informacion personal:</h3>
    <p>Direccion: <?php echo $profile->address;?></p>
    <p>Telefono: <?php echo $profile->telephone;?></p>
    <p>Ciudad: <?php echo $profile->city;?></p>
    <p><?php echo anchor("profiles/edit", "editar")?>
 </div>
 <div id= "reputation">
    <h3>Tu reputacion:</h3>
    <h4>Como comprador:<?php echo $reputation['as_buyer'];?></h4>
    <h4>Compras totales:<?php echo $profile->total_boughts;?></h4>
    <h4>Como vendedor:<?php echo $reputation['as_seller'];?></h4>
    <h4>Ventas totales:<?php echo $profile->total_sales;?></h4>
 </div>
 
    

<?php else:?>
    <?php if($profile->id === get_user(username())->profile->id ):?>
        <?php echo anchor("profiles/edit/".$profile->id, "Editar perfil");?>
    <?php else:?>
        Este usuario aun no completa el perfil
    <?php endif;?>
<?php endif;?>
