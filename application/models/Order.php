<?php

class Order extends ActiveRecord\Model
{   
   /*static $validates_presence_of = array(
         array('user_id')
   );*/
   static $belongs_to = array(
   	array('profile'),
   	array('product')
   );
   
   static $has_many = array(
      array('products')
   );
   
   public function decrease_credit($seller, $order_total)
   {
       $shop = $seller->shop;
       $credit = $shop->credit;
       $credit->ammount = $credit->ammount - (0.03*$order_total);
       $credit->save();      
   }
   
   public function monthly_sales($profile_id, $month = NULL, $year = NULL) {
      // > Establecer mes y año actuales
      if ( is_null($month) )
         $month = date('m');
      
      if ( is_null($year) )
         $year = date('Y');
      
      // > Calcular fecha de inicio de mes y fin de mes
      $date1_unix = strtotime($year.'-'.$month.'-01');
      $date2_unix = strtotime('+1 month', $date1_unix);
      $date2_unix = strtotime('-1 day', $date2_unix);
      
      $date1 = date('Y-m-d', $date1_unix).' 00:00:00';
      $date2 = date('Y-m-d', $date2_unix).' 23:59:59';
      
      // > Magia de ActiveRecord
      // :TODO: Encontrar forma más "ActiveRecord-osa" de hacer COUNT - SUM
      $result = $this -> db -> select_sum('orders.quantity', 'products_sold')
                            -> select_sum('orders.total', 'total_sales')
                            
                            -> select('(COUNT(*) - SUM(orders.completed)) AS ratings_pending', FALSE)
                            
                            -> join('products', 'products.id = orders.product_id')
                            -> join('shops'   , 'shops.id = products.shop_id')
                            
                            -> where('orders.completed', 1)
                            -> where('shops.profile_id', $profile_id)
                            -> where('orders.date >='  , $date1)
                            -> where('orders.date <='  , $date2)
                            
                            -> get('orders');
      
      if ( $result->num_rows() > 0 )
         return $result->row();
      else
         return NULL;
   }
}