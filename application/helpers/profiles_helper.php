<?php
   function get_gravatar($email){   
      $g = md5($email);   
      return ("http://0.gravatar.com/avatar/".$g."?size=115");   
   }
   
   function _has_shop($username){
      $user = User::find_by_username($username);
      if(_has_profile($user)){
         $shop = Shop::find_by_profile_id($user->profile->id);
         return $shop;
      }
      else{
         return false;
      }
      
   }
   
   function _has_profile($user){
       $profile = $user->profile;       
       return ! is_null($profile);
    }
    
    function get_user($username){
        $user = User::find_by_username($username);
        return $user;
        
    }
    
    function check_credit($username){
      $shop =  _has_shop($username);
      if(! is_null($shop)){
         return $shop->credit->ammount;
      }
      else{
         return null;
      }
    }
    
  
   
   

?>