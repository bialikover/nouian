	<div id="content">
		<div id="filtersandcategories">
			<h2>Filtro por categoria</h2>
			<div id="categoryfilter">
			<ul>
			<?php foreach ($categories as $category):?>
				<?php if(count($category->products) > 0):?>
				<li>
					<?php echo anchor("categoria/".$category->name_url ,$category->name); ?>
					<span><?php echo count($category->products);?></span>
				</li>
				<?php endif;?>
			<?php endforeach;?>
			</ul>
			</div>
			<hr>
			<!--<h2>Filtro por lugar</h2>
			<div id="placefilter">
				<ul>
				<li><a href="#">DF</a><span>112</span></li>
				<li><a href="#">Hidalgo</a><span>63</span></li>
				<li><a href="#">Guerrero</a><span>2323</span></li>
				<li><a href="#">Oaxaca</a><span>54</span></li>
				<li><a href="#">Nayarit</a><span>98</span></li>
				<li><a href="#">Colima</a><span>234</span></li>
				<li><a href="#">Veracruz</a><span>2564</span></li>
				<li><a href="#">Puebla</a><span>21</span></li>
				</ul>
			</div>-->
		</div>
		<div id="searchresults"><!--start search results=-->
			<div id="resultsview">
			<h2>Termino de busqueda <span class="highlight"><?php echo $criteria;?></span></h2>
			<!--<span id="resultsviewselect">Ver en: <a class="orange" href="#"><b>grid</b></a> | <a  class="orange" href="#">lista</a> </span>-->
			
			</div>
				<div id="sponsoredresults"> <!--start sponsored search results=-->
				<h3>Destacados</h3>
				<?php foreach ($featured as $feat ):?>					
				
				<div class="productsearchsponsored">
				<?php if($feat->images!= null):?>
                    <?php echo anchor("producto/$feat->name_url",'<img src ="'.base_url()."images/product/370x370". $feat->images[0]->image_url.'" width="237px" height="237px"/>');?>
                <?php else:?>
                    <?php echo anchor("producto/$feat->name_url",'<img src ="'.base_url().'/assets/images/noimage'.rand(0,4).'.png'.'" width="237px" height="237px"/>');?>
                <?php endif;?>	
				</a>
				<span >
				<?php echo anchor("producto/$feat->name_url", $feat->name ,
				array('class'=>'searchproductname','title'=>$feat->name));?>
				</span><br><!--si el nombre del producto tiene mas de 22 caracteres recortar a los 22 y poner 3 punbtos suspensivos=-->	
				<span >
					<?php echo anchor($feat->shop->name_url, $feat->shop->name_url, 
					array('class' => 'searchproductshop', 'title' => $feat->shop->name_url ) );?>
					</span><!--si el nombre de la tienda tiene mas de 11 caracteres recortar a los 22 y poner 3 punbtos suspensivos=-->	
				<span class="searchproductprice">$<?php echo $feat->price;?> MXN</span>
				</div>
				<?php endforeach;?>
			</div>
			
			<div id="normalresults"> <!--sponsored end and start normal search results=-->
			<?php foreach ($products as $product):?>
			
				<div id="normalresultssearch">
				<div class="productsearchnormal">
				<?php if($product->images!= null):?>
                    <?php echo anchor("producto/$product->name_url",'<img src ="'.base_url()."images/product/370x370/". $product->images[0]->image_url.'" width="237px" height="237px"/>');?>
                <?php else:?>
                    <?php echo anchor("producto/$product->name_url",'<img src ="'.base_url().'/assets/images/noimage'.rand(0,4).'.png'.'" width="237px" height="237px"/>');?>
                <?php endif;?>								
				<span >
					<?php echo anchor("producto/$product->name_url", $product->name, 
					array('class' => 'searchproductname', 'title' => $product->name ) );?>
					
				</span><br><!--si el nombre del producto tiene mas de 22 caracteres recortar a los 22 y poner 3 punbtos suspensivos=-->	
				<span >
					<?php echo anchor($product->shop->name_url, $product->shop->name_url, 
					array('class' => 'searchproductshop', 'title' => $product->shop->name_url ) );?>					
				</span><!--si el nombre de la tienda tiene mas de 11 caracteres recortar a los 22 y poner 3 punbtos suspensivos=-->	
				<span class="searchproductprice">$<?php echo $product->price;?> MXN</span>
				</div>
				</div>
			<?php endforeach;?>
			</div><!--end normal search results=-->
			<!--<div id="relatedsearch">
				<h2>Related to <b>termino de busqueda</b></h2>
				<div id="relatedsearchresult">
				<ul>
				<li><a href="#">Ejemplo 1</a></li>
				<li><a href="#">Ejemplo 2</a></li>
				<li><a href="#">Ejemplo 3</a></li>
				</ul>
				<ul>
				<li><a href="#">Ejemplo 4</a></li>
				<li><a href="#">Ejemplo 5</a></li>
				<li><a href="#">Ejemplo 6</a></li>
				</ul>
				<ul>
				<li><a href="#">Ejemplo 7</a></li>
				<li><a href="#">Ejemplo 8</a></li>
				<li><a href="#">Ejemplo 9</a></li>
				</ul>
				</div>
			</div>-->
			<div class="clear"></div>
			<!--<div id="navsearch">
			
			<ul>
			<a href="#"><</a>
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li>...</li>
			<li><a href="#">100</a></li>
			<a href="#">></a>
			<ul>
			
			</div>-->
		</div><!--end search results=-->
		
	</div><!--end content=-->	