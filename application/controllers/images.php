<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images extends CI_Controller {
   public $width_height_allowed = array("full", "100x100", "200x200", "370x370");
   
   public function _get_mimetype_header($filename) {
      $content_type = 'image/';
      $ext          = end(explode('.', $filename));
      
      switch ( $ext ) {
         case 'gif':
            return $content_type.'gif';
         case 'jpg': case 'jpeg':
            return $content_type.'jpeg';
         case 'pjpeg':
            return $content_type.'pjpeg';
         case 'png':
            return $content_type.'png';
         case 'tiff':
            return $content_type.'tiff';
      }
   }
   
   public function _print_image($filename, $filename_res, $width, $height) {
      if ( is_file($filename_res) ) {
         header('HTTP/1.1 200 :)');
         header('Content-Type: '.$this->_get_mimetype_header($filename_res));
         header('Content-Length: '.filesize($filename_res));
         readfile($filename_res);
      } elseif ( is_file($filename) ) {
         $config['image_library']  = 'gd2';
         $config['source_image']   = $filename;
         $config['new_image']      = $filename_res;
         $config['maintain_ratio'] = TRUE;
         
         $config['width']  = $width;
         $config['height'] = $height;
         
         $this->load->library('image_lib', $config);
         
         if ( $this->image_lib->resize() ) {
            header('HTTP/1.1 201 (:');
            header('Content-Type: '.$this->_get_mimetype_header($filename_res));
            header('Content-Length: '.filesize($filename_res));
            readfile($filename_res);
         } else header('HTTP/1.1 500 Nos rompiste el server :(');
      } else header('HTTP/1.1 404 LOL no hay nada :P');
   }
   
   public function product($width_height = NULL, $shop = NULL, $product = NULL, $image = NULL) {
      if ( $width_height == NULL || $shop == NULL || $product == NULL || $image == NULL ) {
         header('HTTP/1.1 403 Chinga tu madre n_n');
         die();
      }
      
      if ( !in_array($width_height, $this->width_height_allowed) ) {
         header('HTTP/1.1 403 Chinga tu madre n_n');
         die();
      }
      
      $filename     = APPPATH.'../assets/images/shops/'.$shop.'/'.$product.'/'.$image;
      $filename_res = APPPATH.'../assets/images/shops/'.$shop.'/'.$product.'/'.$width_height.'-'.$image;
      
      if ( $width_height == 'full' ) {
            header('HTTP/1.1 200 :)');
            header('Content-Type: '.$this->_get_mimetype_header($filename));
            header('Content-Length: '.filesize($filename));
            readfile($filename);
      } else {
         $wh = explode('x', $width_height);
         $this->_print_image($filename, $filename_res, $wh[0], $wh[1]);
      }
   }
   
   public function shop($width_height = NULL, $shop = NULL, $image = NULL) {
      if ( $width_height == NULL || $shop == NULL || $image == NULL) {
         header('HTTP/1.1 403 Hacker detected');
         die();
      }
      
      if ( !in_array($width_height, $this->width_height_allowed) ) {
         header('HTTP/1.1 403 Hacker detected');
         die();
      }
      
      $filename     = APPPATH.'../assets/images/shops/'.$shop.'/'.$image;
      $filename_res = APPPATH.'../assets/images/shops/'.$shop.'/'.$width_height.'-'.$image;
            
      if ( $width_height == 'full' ) {
            header('HTTP/1.1 200 :)');
            header('Content-Type: '.$this->_get_mimetype_header($filename));
            header('Content-Length: '.filesize($filename));
            readfile($filename);
      } else {
         $wh = explode('x', $width_height);
         $this->_print_image($filename, $filename_res, $wh[0], $wh[1]);
      }
   }
}
