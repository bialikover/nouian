<?php
    function RelativeTime($time){
      if(!is_numeric($time))
            $time = strtotime($time);

        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "age");
        $lengths = array("60","60","24","7","4.35","12","100");

        $now = time();

        $difference = $now - $time;
        if ($difference <= 10 && $difference >= 0)
            return $tense = 'just now';
        elseif($difference > 0)
            $tense = 'ago';
        elseif($difference < 0)
            $tense = 'later';

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        $period =  $periods[$j] . ($difference >1 ? 's' :'');
        return "{$difference} {$period} {$tense} ";
    }
  /**
   * validates if a product belogs to a user's shop
   *
   * @return boolean (true or false)
   * @author adanowsky bialikover
   * @param product_id, username
   **/
    function is_owner($product_id, $username){        
        try{
            $user = User::find_by_username($username);
            if($user->profile != null){
      
               $shop = Shop::find_by_profile_id($user->profile->id);                
               $product = Product::find($product_id);
               
               if($shop != null){        
                   if($product->shop->id === $shop->id){
                       return true;
                   }
                   else{
                       return false;
                   }
               }
            else{
               return false;
            }
            }
        }
        catch (ActiveRecord\RecordNotFound $e){
             return false;
        }
    }
 
  
  
?>